-- This is a list of radii for each vehicle. Since barrel4's dont hit a collision detection event when you drive into them, I have to do
-- a manual distance check to take a guess that maybe you were the one to hit them.
-- This is a rough table of each vehicle's size. A dumper can hit a barrel from afar because it is big, but a bike would be miles away
-- A too high number will result in players that aren't that close to be killed anyway
-- A too low number could mean people can just barge through barrels with that car and they wont die
-- Better to go too high imo, since that's the old behavior.

RADII = {
	-- Bikes = 2
	[461] =	2, --"pcj600",
	[462] =	2, --"faggio",
	[463] =	2, --"freeway",
	[448] =	2, --"pizzaboy",
	[468] =	2, --"sanchez",
	[481] =	2, --"bmx",
	[509] =	2, --"bike",
	[510] =	2, --"mtbike",
	[521] =	2, --"fcr900",
	[522] =	2, --"nrg500",
	[523] =	2, --"copbike",
	[581] =	2, --"bf400",
	[586] =	2, --"wayfarer",

	-- Regular cars = 6 or 7
	[400] =	6, --"landstal",
	[401] =	6, --"bravura",
	[402] =	6, --"buffalo",
	[404] =	6, --"peren",
	[405] =	6, --"sentinel",
	[410] =	6, --"manana",
	[411] =	6, --"infernus",
	[412] =	6, --"voodoo",
	[413] =	6, --"pony",
	[414] =	6, --"mule",
	[415] =	6, --"cheetah",
	[418] =	6, --"moonbeam",
	[419] =	6, --"esperant",
	[420] =	6, --"taxi",
	[421] =	6, --"washing",
	[422] =	6, --"bobcat",
	[424] =	6, --"bfinject",
	[426] =	6, --"premier",
	[429] =	6, --"banshee",
	[434] =	6, --"hotknife",
	[436] =	6, --"previon",
	[438] =	6, --"cabbie",
	[439] =	6, --"stallion",
	[440] =	6, --"rumpo",
	[442] =	6, --"romero",
	[445] =	6, --"admiral",
	[451] =	6, --"turismo",
	[457] =	6, --"caddy",
	[458] =	6, --"solair",
	[459] =	6, --"topfun",
	[466] =	6, --"glendale",
	[467] =	6, --"oceanic",
	[470] =	6, --"patriot",
	[474] =	6, --"hermes",
	[475] =	6, --"sabre",
	[477] =	6, --"zr350",
	[478] =	6, --"walton",
	[479] =	6, --"regina",
	[480] =	6, --"comet",
	[482] =	6, --"burrito",
	[483] =	6, --"camper",
	[485] =	6, --"baggage",
	[489] =	6, --"rancher",
	[491] =	6, --"virgo",
	[492] =	6, --"greenwoo",
	[494] =	6, --"hotring",
	[495] =	6, --"sandking",
	[496] =	6, --"blistac",
	[500] =	6, --"mesa",
	[502] =	6, --"hotrina",
	[503] =	6, --"hotrinb",
	[504] =	6, --"bloodra",
	[505] =	6, --"rnchlure",
	[506] =	6, --"supergt",
	[507] =	6, --"elegant",
	[516] =	6, --"nebula",
	[517] =	6, --"majestic",
	[518] =	6, --"buccanee",
	[526] =	6, --"fortune",
	[527] =	6, --"cadrona",
	[528] =	6, --"fbitruck",
	[529] =	6, --"willard",
	[530] =	6, --"forklift",
	[531] =	6, --"tractor",
	[533] =	6, --"feltzer",
	[534] =	6, --"remingtn",
	[535] =	6, --"slamvan",
	[536] =	6, --"blade",
	[539] =	6, --"vortex",
	[540] =	6, --"vincent",
	[541] =	6, --"bullet",
	[542] =	6, --"clover",
	[543] =	6, --"sadler",
	[545] =	6, --"hustler",
	[546] =	6, --"intruder",
	[547] =	6, --"primo",
	[549] =	6, --"tampa",
	[550] =	6, --"sunrise",
	[551] =	6, --"merit",
	[552] =	6, --"utility",
	[554] =	6, --"yosemite",
	[555] =	6, --"windsor",
	[558] =	6, --"uranus",
	[559] =	6, --"jester",
	[560] =	6, --"sultan",
	[561] =	6, --"stratum",
	[562] =	6, --"elegy",
	[565] =	6, --"flash",
	[566] =	6, --"tahoma",
	[567] =	6, --"savanna",
	[568] =	6, --"bandito",
	[574] =	6, --"sweeper",
	[575] =	6, --"broadway",
	[576] =	6, --"tornado",
	[579] =	6, --"huntley",
	[580] =	6, --"stafford",
	[582] =	6, --"newsvan",
	[583] =	6, --"tug",
	[585] =	6, --"emperor",
	[587] =	6, --"euros",
	[589] =	6, --"club",
	[596] =	6, --"copcarla",
	[597] =	6, --"copcarsf",
	[598] =	6, --"copcarvg",
	[599] =	6, --"copcarru",
	[600] =	6, --"picador",
	[602] =	6, --"alpha",
	[603] =	6, --"phoenix",
	[604] =	6, --"glenshit",
	[605] =	6, --"sadlshit",
	[606] =	6, --"bagboxa",
	[607] =	6, --"bagboxb",

	-- Tested Trucks
	[403] =	10, --"linerun",
	[406] =	12, --"dumper",

	-- Guesses
	[407] =	10, --"firetruk",
	[408] =	10, --"trash",
	[409] =	10, --"stretch",
	[416] =	10, --"ambulan",
	[423] =	10, --"mrwhoop",
	[427] =	10, --"enforcer",
	[428] =	10, --"securica",
	[431] =	12, --"bus",
	[433] =	12, --"barracks",
	[437] =	12, --"coach",
	[441] =	2, --"rcbandit",
	[443] =	12, --"packer",
	[444] =	8, --"monster",
	[455] =	12, --"flatbed",
	[456] =	10, --"yankee",
	[464] =	2, --"rcbaron",
	[465] =	2, --"rcraider",
	[471] =	6, --"quad",
	[486] =	10, --"dozer",
	[490] =	8, --"fbiranch",
	[498] =	10, --"boxville",
	[499] =	10, --"benson",
	[501] =	2, --"rcgoblin",
	[508] =	10, --"journey",
	[514] =	10, --"petro",
	[515] =	10, --"rdtrain",
	[524] =	10, --"cement",
	[525] =	8, --"towtruck",
	[532] =	12, --"combine",
	[544] =	10, --"firela",
	[556] =	8, --"monstera",
	[557] =	8, --"monsterb",
	[564] =	2, --"rctiger",
	[571] =	4, --"kart",
	[572] =	4, --"mower",
	[573] =	10, --"duneride",
	[578] =	10, --"dft30",
	[588] =	10, --"hotdog",
	[594] =	2, --"rccam",
	[601] =	10, --"swatvan",
	[609] =	10, --"boxburg",

	-- Unconsidered. Who hits barrels with these?
	[417] =	15, --"leviathn",
	[425] =	15, --"hunter",
	[432] =	15, --"rhino",
	[430] =	15, --"predator",
	[435] =	15, --"artict1",
	[446] =	15, --"squalo",
	[447] =	15, --"seaspar",
	[449] =	15, --"tram",
	[450] =	15, --"artict2",
	[452] =	15, --"speeder",
	[453] =	15, --"reefer",
	[454] =	15, --"tropic",
	[460] =	15, --"skimmer",
	[469] =	15, --"sparrow",
	[472] =	15, --"coastg",
	[473] =	15, --"dinghy",
	[476] =	15, --"rustler",
	[484] =	15, --"marquis",
	[487] =	15, --"maverick",
	[488] =	15, --"vcnmav",
	[493] =	15, --"jetmax",
	[497] =	15, --"polmav",
	[511] =	15, --"beagle",
	[512] =	15, --"cropdust",
	[513] =	15, --"stunt",
	[519] =	15, --"shamal",
	[520] =	15, --"hydra",
	[537] =	15, --"freight",
	[538] =	15, --"streak",
	[548] =	15, --"cargobob",
	[553] =	15, --"nevada",
	[563] =	15, --"raindanc",
	[569] =	15, --"freiflat",
	[570] =	15, --"streakc",
	[577] =	15, --"at400",
	[584] =	15, --"petrotr",
	[590] =	15, --"freibox",
	[591] =	15, --"artict3",
	[592] =	15, --"androm",
	[593] =	15, --"dodo",
	[595] =	15, --"launch",
	[608] =	15, --"tugstair",
	[610] =	15, --"farmtr1",
	[611] =	15, --"utiltr1"

}
