--[[local popupColors = {
	tocolor(86, 101, 120, 180), -- Background
	tocolor(0, 0, 0, 255), -- Shadow of the Icon
	tocolor(240, 240, 240, 255) -- Text
}]]

local popupSound
local popupColors = {
	tocolor(0, 0, 0, 170), -- Background
	tocolor(0, 0, 0, 255), -- Shadow of the Icon
	tocolor(175, 202, 230, 255) -- Text
}

-- Rendering
local renderPool = {}
local boxZ = 0

local screenX, screenY = guiGetScreenSize()
local boxSize = { screenY * 4.5 / 11.5, screenY / 11.5, screenY * 5.1 / 11.5, screenY / 11.5 } -- posX, posY, width, height 480 93.9

-- GUI
local window, tabPanel, exitButton
local guiIcons = {}
local tooltip
local receivedData

function update()	
	for i = #renderPool, 1, -1 do
		if getTickCount() > renderPool[i][1] + 10000 then table.remove(renderPool, i) end
	end
end

function dxDrawBorderedText(outline, text, left, top, right, bottom, color, scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    if type(scaleY) == "string" then
        scaleY, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY = scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX
    end
    local outlineX = (scaleX or 1) * (1.333333333333334 * (outline or 1))
    local outlineY = (scaleY or 1) * (1.333333333333334 * (outline or 1))
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outlineX, top - outlineY, right - outlineX, bottom - outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outlineX, top - outlineY, right + outlineX, bottom - outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outlineX, top + outlineY, right - outlineX, bottom + outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outlineX, top + outlineY, right + outlineX, bottom + outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outlineX, top, right - outlineX, bottom, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outlineX, top, right + outlineX, bottom, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left, top - outlineY, right, bottom - outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left, top + outlineY, right, bottom + outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text, left, top, right, bottom, color, scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
end

addEventHandler("onClientResourceStart", resourceRoot, function()
	if isTimer(updateTimer) then killTimer(updateTimer) end
	updateTimer = setTimer(update, 50, 0)
end )

addEvent("showPopup", true)
addEventHandler("showPopup", getRootElement(), function(group, achievement, percentage, specialData)
	-- visuals
	table.insert(renderPool, {getTickCount(), group, achievement, percentage})
	
	-- sound
	if isElement(popupSound) then destroyElement(popupSound) end
	popupSound = playSound("s.mp3")
	setSoundVolume(popupSound, 0.5)
end )

addEventHandler("onClientRender", root, function()
	for i = #renderPool, 1, -1 do
		-- Background
		dxDrawRectangle(boxSize[1], screenY - boxSize[2] * i * 1.2, boxSize[3], boxSize[4], popupColors[1])
		
		-- Icon
		dxDrawRectangle(boxSize[1] + boxSize[4]*0.085, screenY - boxSize[2] * i * 1.2 + boxSize[4]*0.085, boxSize[4]*0.87, boxSize[4]*0.87, popupColors[2])
		dxDrawImage(boxSize[1] + boxSize[4]*0.075, screenY - boxSize[2] * i * 1.2 + boxSize[4]*0.075, boxSize[4]*0.85, boxSize[4]*0.85, dxCreateTexture("icons/" ..achievementsList[renderPool[i][2]][renderPool[i][3]][1]), 0, 0, 0, tocolor(255, 255, 255, 255))
		
		-- Name
		dxDrawBorderedText(1, achievementsList[renderPool[i][2]][renderPool[i][3]][2], boxSize[1] + boxSize[4] + 3, screenY - boxSize[2] * i * 1.2 - boxSize[4]/6.5 + 3, screenX, screenY, popupColors[2], screenY/800, "beckett", "left", "top", true, false)
		dxDrawBorderedText(1, achievementsList[renderPool[i][2]][renderPool[i][3]][2], boxSize[1] + boxSize[4], screenY - boxSize[2] * i * 1.2 - boxSize[4]/6.5, screenX, screenY, popupColors[3], screenY/800, "beckett", "left", "top", true, false)
		
		-- Description
		dxDrawBorderedText(1, achievementsList[renderPool[i][2]][renderPool[i][3]][4], boxSize[1] + boxSize[4], screenY - boxSize[2] * i * 1.2 + boxSize[4]/3.8, screenX, screenY, popupColors[3], screenY/1400, "bankgothic", "left", "top", true, false)
		
		-- How many players got
		dxDrawBorderedText(1, string.format("%.1f", renderPool[i][4]).. "% of players got this achievement", boxSize[1] + boxSize[4], screenY - boxSize[2] * i * 1.2 + boxSize[4]/1.3, screenX, screenY, popupColors[3], screenY/1900, "bankgothic", "left", "top", true, false)
	end
end )

function showAchievements()
	if isElement(window) then 
		destroyElement(window)
		if isElement(tooltip) then destroyElement(tooltip) end
		if isElement(tooltipWindow) then destroyElement(tooltipWindow) end
		showCursor(false)
	else 
		showCursor(true)
		
		-- Create Window
		window = guiCreateWindow(screenY/screenX * 0.35, 0.15, screenY/screenX * 0.7, 0.7, "Achievements", true)
		guiWindowSetSizable(window, false)
		tabPanel = guiCreateTabPanel(0, 0.05, 1, 0.88, true, window)
		exitButton = guiCreateButton(0.85, 0.95, 0.15, 0.2, "Cancel", true, window)
		addEventHandler("onClientGUIClick", exitButton, function()
			destroyElement(window)
			destroyElement(tooltip)
			destroyElement(tooltipWindow)
			showCursor(false)
		end )
		
		-- Request data from DATABASE
		triggerServerEvent("getAchievements", localPlayer)
	end
end
addCommandHandler("achievements", showAchievements)
addCommandHandler("ach", showAchievements)
bindKey("F1", "down", showAchievements)

addEvent("getAchievementsFromServer", true)
addEventHandler("getAchievementsFromServer", getRootElement(), function(data, leaderboard)
	if not isElement(window) then return end
	receivedData = data
	
	local total, unlocked = 0, 0;
	for k, v in pairs(achievementsList) do
		for i = 1, #achievementsList[k] do
			total = total + 1
			if receivedData[k][i][1] == true then unlocked = unlocked + 1 end
		end		
	end	
	local info = guiCreateLabel(0.05, 0.95, 0.5, 0.2, "Achievements unlocked: " ..unlocked.. "/" ..total, true, window) 
	
	guiSetAlpha(window, 0.7)
	guiSetAlpha(tabPanel, 0.7)
	
	-- Tooltip	
	tooltipWindow = guiCreateWindow(0, 0, 1, 1, "", true)
	guiSetVisible(tooltipWindow, false)
	guiSetEnabled(tooltipWindow, false)
	guiSetAlpha(tooltipWindow, 0.8)
	
	guiBringToFront(tooltipWindow)
	
	tooltip = guiCreateLabel(0, 0, 1, 1, "", true)
	guiSetVisible(tooltip, false)
	guiSetEnabled(tooltip, false)
	guiBringToFront(tooltip)
	
	-- Create Tabs
	local tabs = {}
	local scrollPanes = {}
	for k, v in pairs(achievementsList) do
		tabs[k] = guiCreateTab(k, tabPanel)
		scrollPanes[k] = guiCreateScrollPane(0.02, 0.02, 0.98, 1, true, tabs[k])
		
		guiIcons[k] = {}
		local cols = 0
		local r = 1
		for i = 1, #achievementsList[k] do
			if data[k][i][1] then guiIcons[k][i] = guiCreateStaticImage(0.03 + 0.12*r - 0.12, 0.05 + cols * 0.12, 0.1, 0.1, "icons/" ..achievementsList[k][i][1], true, scrollPanes[k])
			else guiIcons[k][i] = guiCreateStaticImage(0.03 + 0.12*r - 0.12, 0.05 + cols * 0.12, 0.1, 0.1, "icons/gray/" ..achievementsList[k][i][1], true, scrollPanes[k]) end
			
			-- Next row of achievements 
			r = r + 1
			if 0.03 + 0.12 * r > 1 then 
				cols = cols + 1 
				r = 1
			end
		end
		
		-- Enable scroll bar if there is too much achievements
		if cols * 0.12 + 0.05 > 1 then guiScrollPaneSetScrollBars(scrollPanes[k], false, true) end
	end
	
	tabs["Leaderboard"] = guiCreateTab("Leaderboard", tabPanel)
	guiSetFont(guiCreateLabel(0.05, 0.05, 0.5, 0.2, "№", true, tabs["Leaderboard"]), "default-bold-small")
	guiSetFont(guiCreateLabel(0.10, 0.05, 0.5, 0.2, "Player", true, tabs["Leaderboard"]), "default-bold-small")
	guiSetFont(guiCreateLabel(0.70, 0.05, 0.5, 0.2, "Achivements Done", true, tabs["Leaderboard"]), "default-bold-small")
	
	for i = 1, 40 do
		if i > #leaderboard then
			--guiSetFont(guiCreateLabel(0.05, 0.1 + 0.02*i, 0.5, 0.2, i.. ".", true, tabs["Leaderboard"]), "default-bold-small")
			--guiSetFont(guiCreateLabel(0.10, 0.1 + 0.02*i, 0.5, 0.2, "None", true, tabs["Leaderboard"]), "default-bold-small")
			--guiSetFont(guiCreateLabel(0.75, 0.1 + 0.02*i, 0.5, 0.2, "0/" ..total, true, tabs["Leaderboard"]), "default-bold-small")
		else
			guiSetFont(guiCreateLabel(0.05, 0.1 + 0.02*i, 0.5, 0.2, i.. ".", true, tabs["Leaderboard"]), "default-bold-small")
			guiSetFont(guiCreateLabel(0.10, 0.1 + 0.02*i, 0.5, 0.2, leaderboard[i]["playername"]:gsub("#%x%x%x%x%x%x", ""), true, tabs["Leaderboard"]), "default-bold-small")
			guiSetFont(guiCreateLabel(0.75, 0.1 + 0.02*i, 0.5, 0.2, leaderboard[i]["count"].. "/" ..total, true, tabs["Leaderboard"]), "default-bold-small")
		end
	end
end )

addEventHandler("onClientMouseLeave", getRootElement(), function(aX, aY, guiElement)
	if guiElement == nil or not isElement(tooltip) then return end
	if getElementType(guiElement) ~= "gui-staticimage" then
		guiSetVisible(tooltip, false) 
		guiSetVisible(tooltipWindow, false) 
	elseif getElementType(guiElement) == "gui-staticimage" then 
		for k, v in pairs(achievementsList) do
			if guiIcons[k] == nil then break end
			for i = 1, #guiIcons[k] do
				if guiElement == guiIcons[k][i] then
					guiSetVisible(tooltip, true)
					guiSetVisible(tooltipWindow, true)
					
					guiBringToFront(tooltipWindow)
					guiBringToFront(tooltip)
					
					local ach_state = ""
					if receivedData ~= nil then
						if receivedData[k][i][1] == true then ach_state = "Unlocked (" ..os.date("%d.%m.%Y", receivedData[k][i][4]).. ")"
						else ach_state = "Locked" end
					end
					
					if achievementsList[k][i][3] > 1 and receivedData ~= nil then
						guiSetText(tooltip, achievementsList[k][i][2].. "\n" ..achievementsList[k][i][4].. "\n\n" ..ach_state.. "\n\nProgress: " ..receivedData[k][i][2].. "/" ..achievementsList[k][i][3].. "\n\n" ..string.format("%.1f", receivedData[k][i][3]).. "% of players got this achievement")
					else
						guiSetText(tooltip, achievementsList[k][i][2].. "\n" ..achievementsList[k][i][4].. "\n\n" ..ach_state.. "\n\n" ..string.format("%.1f", receivedData[k][i][3]).. "% of players got this achievement")
					end
					
					local sizeX = guiLabelGetTextExtent(tooltip)
					local sizeY = guiLabelGetFontHeight(tooltip)
					
					if achievementsList[k][i][3] > 1 and receivedData ~= nil then sizeY = sizeY * 8
					else sizeY = sizeY * 6 end
					
					guiSetSize(tooltipWindow, sizeX + 15, sizeY + 15, false)
					
					break
				end
			end
		end
	end
end )

addEventHandler("onClientMouseEnter", getRootElement(), function(aX, aY, guiElement)
	if guiElement == nil or not isElement(tooltip) then return end
	if getElementType(guiElement) == "gui-staticimage" then 
		guiSetVisible(tooltip, false) 
		guiSetVisible(tooltipWindow, false) 
	end
end )

addEventHandler("onClientCursorMove", root, function(_, _, x, y)
	if isElement(window) and isElement(tooltipWindow) and isElement(tooltip) then
        guiSetPosition(tooltipWindow, x + 5, y, false)
        guiSetPosition(tooltip, x + 10, y + 10, false)
	end
end )

addEventHandler("onClientKey", root, function(button, press)
	if not isChatBoxInputActive() then setElementData(localPlayer, "ach_keyPresses", (getElementData(localPlayer, "ach_keyPresses") or 0) + 1) end
end )

-- DEBUG
--[[local test = 16
bindKey("num_0", "down", function()
	table.insert(renderPool, {getTickCount(), "general", test, 69})
	test = test + 1
end )]]