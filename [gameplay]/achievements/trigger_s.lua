local g_currentMap = getResourceName(call(getResourceFromName("mapmanager"), "getRunningGamemodeMap"))

local randomizerSlowCars = {}

-- triggerAchievement
-- playerSource, group of achievements, achievement, data 

function update()
	for _, players in ipairs(getElementsByType("player")) do
		if getPedOccupiedVehicle(players) then
			if (Vector3(getElementVelocity(getPedOccupiedVehicle(players))).length * 160) >= 350 then
				triggerAchievement(players, "general", 2, nil)
			end
		end
		
		-- AFK
		if getPlayerIdleTime(players) and getPlayerIdleTime(players) >= 300000 then
			triggerAchievement(players, "general", 20, nil)
		end
		
		-- Explorer
		local x, y, z = getElementPosition(players)
		local zone = getZoneName(x, y, z, false)
		if zone ~= "Unknown" and zone ~= "Whetstone" and zone ~= "Flint County" and zone ~= "Los Santos"
		and zone ~= "San Fierro" and zone ~= "Red County" and zone ~= "Las Venturas" and zone ~= "Bone County" then
			updateObjective(players, "general", 22, zone)
		end
	end
end

addCommandHandler("rate", function(player, command, rated)
	rated = tonumber(rated)
	if rated then
		if rated <= 10 and rated >= 0 then updateObjective(player, "general", 21, g_currentMap) end
	end
end )

addEvent("onPlayerFinish", true)
addEventHandler("onPlayerFinish", getRootElement(), function(rank, time)
	-- Finish any map
	triggerAchievement(source, "general", 1, nil)
	
	-- Finish 100 maps
	triggerAchievement(source, "general", 2, nil)
	
	-- Finish 500 maps
	triggerAchievement(source, "general", 3, nil)
	
	-- Finish any map without key presses
	if getElementData(source, "ach_keyPresses") == 0 then
		triggerAchievement(source, "general", 15, nil)
	end
	
	-- Finish any map on foot
	if not getPedOccupiedVehicle(source) then
		triggerAchievement(source, "general", 17, nil)
	end
	
	-- Confiditions requires being in the vehicle below
	if not getPedOccupiedVehicle(source) then return end 
	
	-- Finish Race as passenger
	if getPedOccupiedVehicleSeat(source) ~= 0 then
		triggerAchievement(source, "general", 16, nil)
	end
	
	-- Squalo II
	if g_currentMap == "SqualoII" and getElementModel(getPedOccupiedVehicle(source)) == 460 then
		triggerAchievement(source, "disco", 1, nil)
	end
	
	-- Super GT Training Course
	if g_currentMap == "SuperGTTrainingCourse" and getElementData(source, "timesDied") == 0 then
		triggerAchievement(source, "disco", 6, nil)
	end
	
	-- Deadline
	if g_currentMap == "Deadline" and getElementData(source, "timesDied") == 0 and time <= 135000 then
		triggerAchievement(source, "general", 5, nil)
	end
	
	-- Crazy Taxi
	if g_currentMap == "CrazyTaxi" then
		if getElementData(source, "Score") >= 2500 then triggerAchievement(source, "disco", 8, nil)
		elseif getElementData(source, "Score") == 0 then triggerAchievement(source, "disco", 9, nil) end
	end
	
	-- Fear the Repo
	if g_currentMap == "FearTheRepo" and getElementData(getPedOccupiedVehicle(source), "tvs") >= 14 then
		triggerAchievement(source, "disco", 3, nil)
	end
	
	-- GTA Advance Race
	if g_currentMap == "GTAAdvanceRace" and time < 60000 then
		triggerAchievement(source, "disco", 2, nil)
	end
	
	-- Pedal to Medal Wuzi's maps
	if g_currentMap == "PedalForAMedal3Party" or g_currentMap == "PedalForAMedal1Escape" or g_currentMap == "PedalForAMedal2Meeting" then
		if time <= 120000 then updateObjective(source, "general", 8, g_currentMap) end
	end
	
	-- Procedurally Generated Race
	if g_currentMap == "ProcedurallyGeneratedRace" then
		updateObjective(source, "disco", 10, getElementModel(getPedOccupiedVehicle(source)))
	end
	
	-- OG Races
	if g_currentMap == "Barnstorming" or g_currentMap == "ChopperCheckpoint" or g_currentMap == "HeliHell" or g_currentMap == "MilitaryService" or g_currentMap == "WhirlyBirdWaypoint" or g_currentMap == "WorldWarAce" or
	g_currentMap == "BackroadWanderer" or g_currentMap == "CityCircuit" or g_currentMap == "Freeway" or g_currentMap == "IntoTheCountry" or g_currentMap == "LittleLoop" or g_currentMap == "LowriderRace" or g_currentMap == "Vinewood" or 
	g_currentMap == "DamRider" or g_currentMap == "DesertTricks" or g_currentMap == "LvRingroad" or g_currentMap == "SfToLv" or 
	g_currentMap == "BanditoCounty" or g_currentMap == "CountryEndurance" or g_currentMap == "DirtbikeDanger" or g_currentMap == "GoGoKarting" or g_currentMap == "SanFierroFastlane" or g_currentMap == "SanFierroHills" then
		updateObjective(source, "general", 9, g_currentMap)
	end
	
	-- Chiliad Challenge
	if g_currentMap == "BirdseyeWinder" or g_currentMap == "CobraRun" or g_currentMap == "ScotchBonnetYellowRoute" then
		updateObjective(source, "general", 10, g_currentMap)
	end
	
	-- I wanna find my destiny
	if g_currentMap == "IWannaFindMyDestiny" and getElementData(source, "timesDied") == 0 then
		triggerAchievement(source, "general", 11, nil)
	end
	
	-- WUZIMU as Wuzi
	if g_currentMap == "WuZiMuButAsWoozie" then
		triggerAchievement(source, "disco", 13, nil)
	end
	
	-- SlowRide
	if g_currentMap == "SlowRide" and getElementData(source, "timesDied") == 0 then
		triggerAchievement(source, "disco", 14, nil)
	end
	
	-- Tram VS Feltzer
	if g_currentMap == "TramVsFeltzer" and getVehicleType(getPedOccupiedVehicle(source)) == "Train" then
		setTimer(function(player) 
			if isTrainDerailed(getPedOccupiedVehicle(player)) then
				triggerAchievement(player, "disco", 15, nil)
			end
		end, 2000, 1, source)
	end
	
	-- 15 different air maps
	if getVehicleType(getPedOccupiedVehicle(source)) == "Plane" or getVehicleType(getPedOccupiedVehicle(source)) == "Helicopter" then
		updateObjective(source, "general", 18, g_currentMap)
	end
	
	-- Catalina Quadrilogy
	if g_currentMap == "CatalinaQuadrilogy" then
		triggerAchievement(source, "general", 23, nil)
	end
	
	-- Sunnyside Taxis
	if g_currentMap == "SunnysideTaxis" then
		-- Casual Driver
		if getElementModel(getPedOccupiedVehicle(source)) == 420 then
			triggerAchievement(source, "disco", 16, nil)
		end
	end
end )

-- This function updates objective of achievement that have multiple objectives
-- It checks if insertedData already exists in data section in database in player record and if it doesn't
-- it will add insertedData into database and increment stage, if this insertedData already exists
-- code will do nothing. This is useful for "do these maps for achievement" or "drive these cars" and etc.
function updateObjective(player, group, achievement, insertedData)
	-- Register in database
	local data = getAchievementData(player, group, achievement)
	if data then
		-- If player already made some progress
		data = fromJSON(data)
		
		local found = false
		for j = 1, #data do
			if data[j] == insertedData then found = true end
		end
		
		if not found then 
			-- Trigger next stage
			table.insert(data, insertedData)
			
			triggerAchievement(player, group, achievement, data)
		end
	else
		-- First stage
		triggerAchievement(player, group, achievement, {insertedData})
	end
end

addEventHandler("onElementModelChange", root, function(oldModel, newModel)
	if getElementType(source) == "vehicle" and oldModel and newModel then
		local player = getVehicleOccupant(source, 0)
		if player then
			-- If its a vehicle and somebody driving it
			if getElementData(player, "ach_slowCar") == 0 then
				-- No car stored yet
				setElementData(player, "ach_slowCar", oldModel)
			else
				-- Old-old car stored already
				local oldOldModel = getElementData(player, "ach_slowCar")
				
				if g_currentMap == "GrandRallyRandomizer254CPKEKW" or g_currentMap == "RandomizerRace" or g_currentMap == "RandomizerRace2" or 
				g_currentMap == "RandomizerRace3" or g_currentMap == "Shanty8TrackRandomiser" or g_currentMap == "Shanty8TrackRandomiserCollisions" 
				or g_currentMap == "HoldWRandomiser" or g_currentMap == "MonsterRandomizer" or g_currentMap == "RandomizerRaceRerollEdition" 
				or g_currentMap == "RandomizerOvalRace" or g_currentMap == "SputsBoneCountyRomp" or g_currentMap == "Real8TrackRandomizerExperience" then -- Randomizers maps
					if oldOldModel == oldModel and oldModel == newModel then
						triggerAchievement(player, "general", 24, nil) -- Triple
					else
						local slowCars = {485, 574, 530, 486, 532, 531, 583, 572, 606, 607, 594, 441, 457, 571}
						local isASlowCar = {false, false, false}
						local isAFastCar = {false, false, false}
						
						-- check if it's a junk car
						for i = 1, #slowCars do
							if oldOldModel == slowCars[i] then isASlowCar[1] = true end
							if oldModel == slowCars[i] then isASlowCar[2] = true end
							if newModel == slowCars[i] then isASlowCar[3] = true end
						end
						
						-- check if it's a fast car
						local handling = {getOriginalHandling(oldOldModel), getOriginalHandling(oldModel), getOriginalHandling(newModel)}
						for i = 1, 3 do
							if handling[i]["maxVelocity"] > 190 then isAFastCar[i] = true end
						end
						
						 -- Never Lucky
						if isASlowCar[1] and isASlowCar[2] and isASlowCar[3] then
							triggerAchievement(player, "general", 25, nil)
						end
						
						-- Always Lucky
						if isAFastCar[1] and isAFastCar[2] and isAFastCar[3] then
							triggerAchievement(player, "general", 26, nil) 
						end
						
						-- Update Old-old
						setElementData(player, "ach_slowCar", oldModel)
					end
				end
			end
		end
	end
end )

addEventHandler("onPlayerLogin", root, function()
	triggerAchievement(source, "general", 14, nil)
end )

addEventHandler("onPlayerWasted", getRootElement(), function()
	setElementData(source, "timesDied", (getElementData(source, "timesDied") or 0) + 1)
end )

addEventHandler("onMapStarting", root, function()
	local currentMap = call(getResourceFromName("mapmanager"), "getRunningGamemodeMap")
	if currentMap then g_currentMap = getResourceName(currentMap) end
end )

addEventHandler("onResourceStart", resourceRoot, function()
	if isTimer(updateTimer) then killTimer(updateTimer) end
	updateTimer = setTimer(update, 5000, 0)
end )

addEventHandler("onElementDataChange", root, function(theKey, oldValue, newValue)
	if getElementType(source) == "player" and theKey == "ach_topTime1" and newValue > (oldValue or 0) then
		triggerAchievement(source, "general", 4, nil)
		
		if newValue >= 2 then triggerAchievement(source, "general", 6, nil) end
	end
	
	if getElementType(source) == "player" and theKey == "quizScore" and g_currentMap == "AWiseManFromTheEast" and getPlayerCount() > 1 then
		if newValue > (oldValue or 0) then
			setElementData(source, "timesDied", (getElementData(source, "timesDied") or 0) + 1) -- used as score
			if getElementData(source, "timesDied") == 5 then
				triggerAchievement(source, "general", 13, nil) 
			end
		elseif newValue < (oldValue or 0) then setElementData(source, "timesDied", 0) end -- reset "score"
	end
end )