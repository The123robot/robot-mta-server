local r = getResourceRootElement(getThisResource())

addEventHandler( "onClientResourceStart", r,
function()
	setWorldSpecialPropertyEnabled("extraairresistance", false)
	--outputChatBox("Notice: Extra air resistance is disabled on this map")
end)

addEventHandler( "onClientResourceStop", r,
function()
	setWorldSpecialPropertyEnabled("extraairresistance", true)
end)

addEvent ( "onClientMapStarting", true )
addEventHandler("onClientMapStarting", getRootElement(),
function()
	setWorldSpecialPropertyEnabled("extraairresistance", false)
	outputChatBox("Notice: Extra air resistance is disabled on this map")
end)