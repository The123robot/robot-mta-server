-- Settings
local STUNT_MIN_DISTANCE = 40.0
local STUNT_MIN_HEIGHT = 6.0

local stuntDisabled = false
local worldStateTimer

-- Screen stuff
local screenX, screenY = guiGetScreenSize()

-- Stunts
local taxiInAir = false
local stuntTextMode = true
local stuntPrevPos = {0.0, 0.0, 0.0}
local stuntPrevRot = {0.0, 0.0, 0.0}
local stunts = 0
local stuntCarUpsideDown = false
local stuntRolls = 0
local stuntRot = 0
local updateWheelStateTimer = nil

-- Anti-Teleport Money
local oldPos = {nil, nil, nil}
local showDPos = 0

function updateWheelState()
	local x, y, z = getElementPosition(localPlayer)
	
	-- Teleport prevention
	if oldPos[1] == nil then 
		-- First init
		oldPos[1] = x
		oldPos[2] = y
		oldPos[3] = z
		
		DPos = 0
	else
		-- calculate position difference 
		DPos = math.abs(x - oldPos[1]) + math.abs(y - oldPos[2]) + math.abs(z - oldPos[3])
		
		-- update coords
		oldPos[1] = x
		oldPos[2] = y
		oldPos[3] = z
	end
	
	local taxi = getPedOccupiedVehicle(localPlayer)
	if getElementData(localPlayer, "state") == "spectating" or z > 10000 or not taxi then return end
	if stuntTextMode then return end
	
	-- Check for vehicle types
	if getVehicleType(taxi) == "Train" or getVehicleType(taxi) == "Plane" or getVehicleType(taxi) == "Helicopter" or getVehicleType(taxi) == "Boat" or DPos > 100 
	or stuntDisabled or math.abs(getGravity() - 0.008) > 0.001 then
		taxiInAir = false
		stunts = 0
		return
	end
	
	if not isVehicleWheelOnGround(taxi, "front_left") and not isVehicleWheelOnGround(taxi, "rear_left") and not isVehicleWheelOnGround(taxi, "front_right") and not isVehicleWheelOnGround(taxi, "rear_right") then
		if not taxiInAir and not isVehicleUpsideDown(taxi) then
			stuntPrevPos[1], stuntPrevPos[2], stuntPrevPos[3] = getElementPosition(taxi)
			stuntPrevRot[1], stuntPrevRot[2], stuntPrevRot[3] = getElementRotation(taxi)
			
			stunts = 0
			stuntRolls = 0
			stuntRot = 0
			taxiInAir = true
			stuntCarUpsideDown = false
		else -- already in Air
			-- Flips
			if isVehicleUpsideDown(taxi) and not stuntCarUpsideDown then
				stuntCarUpsideDown = true
			end
			
			if not isVehicleUpsideDown(taxi) and stuntCarUpsideDown then
				stuntCarUpsideDown = false
				stuntRolls = stuntRolls + 1
			end
			
			-- Rotations
			local rX, rY, rZ = getElementRotation(taxi)
			local heading_difference = rZ - stuntPrevRot[3]
			local heading_difference_temp = 0.0
			
			if heading_difference > 180.0 then
				heading_difference_temp = heading_difference
				heading_difference = 360.0 - heading_difference_temp
			else
				if heading_difference < -180.0 then
					heading_difference_temp = heading_difference
					heading_difference = 360.0 + heading_difference_temp
				end
			end
			
			if heading_difference < 0.0 then
				heading_difference_temp = heading_difference
				heading_difference = 0.0 - heading_difference_temp
			end

			stuntRot = stuntRot + heading_difference
		end
	else
		if taxiInAir then	
			local sX, sY, sZ = getElementPosition(taxi)
			local rX, rY, rZ = getElementRotation(taxi)
			local stuntData = {0, 0, 0, 0}
			
			-- Distance > 40 meters
			if getDistanceBetweenPoints2D(sX, sY, stuntPrevPos[1], stuntPrevPos[2]) > STUNT_MIN_DISTANCE then
				stunts = stunts + 1
			end
			
			-- Height > 6 meters
			if stuntPrevPos[3] - sZ > STUNT_MIN_HEIGHT then 
				stunts = stunts + 1
			end
			
			-- 2 ROLLS/FLIPS IN MID AIR
			if stuntRolls > 1 then
				stunts = stunts + 1
			end
			
			-- Rotation > 360 dgr
			if stuntRot > 360.0 then
				stunts = stunts + 1
			end
			
			-- Records
			stuntData[1] = getDistanceBetweenPoints2D(sX, sY, stuntPrevPos[1], stuntPrevPos[2])
			stuntData[2] = math.abs(stuntPrevPos[3] - sZ)
			stuntData[3] = stuntRolls
			stuntData[4] = stuntRot
			
			if stunts > 0 then
				-- Money
				local moneyReward = math.abs(math.floor((stuntRolls * 180 + math.floor(stuntRot*10)/10 + (math.floor(getDistanceBetweenPoints2D(sX, sY, stuntPrevPos[1], stuntPrevPos[2])*10)/10) * 6 + (math.floor((stuntPrevPos[3] - sZ)*10)/10) * 45) * stunts / 15))
				givePlayerMoney(moneyReward)
				setElementData(localPlayer, "Money", getElementData(localPlayer, "Money") + moneyReward)
			
				-- Process text
				if stunts == 1 then
					stuntText = "INSANE STUNT BONUS:"
				elseif stunts == 2 then
					stuntText = "DOUBLE INSANE STUNT BONUS:"
				elseif stunts == 3 then
					stuntText = "TRIPLE INSANE STUNT BONUS:"
				elseif stunts == 4 then
					stuntText = "QUADRUPLE INSANE STUNT BONUS:"
				end
				
				stuntText = stuntText .. " $" .. moneyReward
				stuntTextMode = true
				
				setTimer(function()
					stuntText = "Distance: " ..(math.floor(getDistanceBetweenPoints2D(sX, sY, stuntPrevPos[1], stuntPrevPos[2])*10)/10).. "m Height: " ..math.abs((math.floor((stuntPrevPos[3] - sZ)*10)/10)).. "m Flips: " ..stuntRolls.. " Rotation: " ..math.floor(stuntRot).. "°"
				end, 3500, 1)
				setTimer(function() stuntTextMode = false end, 7000, 1)
				
				triggerServerEvent("checkStunt", getLocalPlayer(), 1, stuntData)
			end
			
			-- cleanup
			taxiInAir = false
			stunts = 0
		end
	end
end

local WORLD_DOWN = {0, 0, -1} 
local UPSIDE_DOWN_THRESHOLD = math.cos(math.rad(80))
  
function isVehicleUpsideDown(vehicle) 
    local matrix = getElementMatrix(vehicle) 
    local vehicleUp = {matrix_rotate (matrix, 0, 0, 1)} 
    local dotP = math.dotP (vehicleUp, WORLD_DOWN) 
    return (dotP >= UPSIDE_DOWN_THRESHOLD) 
end

function matrix_rotate(matrix, x, y, z) 
    local tx = x * matrix[1][1] + y * matrix[2][1] + z * matrix[3][1]   
    local ty = x * matrix[1][2] + y * matrix[2][2] + z * matrix[3][2]   
    local tz = x * matrix[1][3] + y * matrix[2][3] + z * matrix[3][3]   
    return tx, ty, tz 
end
  
function math.dotP(v1, v2) 
    return v1[1]*v2[1] + v1[2]*v2[2] + v1[3]*v2[3] 
end

function dxDrawBorderedText(outline, text, left, top, right, bottom, color, scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    if type(scaleY) == "string" then
        scaleY, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY = scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX
    end
    local outlineX = (scaleX or 1) * (1.333333333333334 * (outline or 1))
    local outlineY = (scaleY or 1) * (1.333333333333334 * (outline or 1))
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outlineX, top - outlineY, right - outlineX, bottom - outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outlineX, top - outlineY, right + outlineX, bottom - outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outlineX, top + outlineY, right - outlineX, bottom + outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outlineX, top + outlineY, right + outlineX, bottom + outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outlineX, top, right - outlineX, bottom, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outlineX, top, right + outlineX, bottom, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left, top - outlineY, right, bottom - outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left, top + outlineY, right, bottom + outlineY, tocolor (0, 0, 0, 225), scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text, left, top, right, bottom, color, scaleX, scaleY, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
end

-- Cancel stunt bonus if vehicle is wrecked
addEventHandler("onClientVehicleDamage", getRootElement(), function(a, b, loss)
	local taxi = getPedOccupiedVehicle(localPlayer)
	if not taxi then return end 
	
	if taxi == source and taxiInAir then
		if (getVehicleType(taxi) == "Automobile" and not (isVehicleWheelOnGround(taxi, "front_left") or isVehicleWheelOnGround(taxi, "rear_left") or isVehicleWheelOnGround(taxi, "front_right") or isVehicleWheelOnGround(taxi, "rear_right"))) or (getVehicleType(taxi) == "Bike" and not (isVehicleWheelOnGround(taxi, "front_left") or isVehicleWheelOnGround(taxi, "rear_left"))) then
			stunts = 0
			taxiInAir = false
		end
	end
end )

addEventHandler("onClientResourceStart", getRootElement(), function(resource)
	if getResourceName(resource) == "stuntbonus" then 
		setElementData(localPlayer, "Money", 0)
		return 
	end
	
	-- Init with delay
	setTimer(function()
		if not isTimer(updateWheelStateTimer) then 
			updateWheelStateTimer = setTimer(updateWheelState, 200, 0)
			setTimer(function() stuntTextMode = false end, 5000, 1) -- cooldown at the start
		end
	end, 15000, 1)
	
	if isTimer(worldStateTimer) then killTimer(worldStateTimer) end
	worldStateTimer = setTimer(function()
		if isWorldSpecialPropertyEnabled("aircars") then stuntDisabled = true	
		else stuntDisabled = false end
	end, 5000, 0)
end )

addEventHandler("onClientResourceStop", getRootElement(), function(resource)
    if getResourceName(resource) == "stuntbonus" then return end
	if isTimer(updateWheelStateTimer) then killTimer(updateWheelStateTimer) end
end )

addEventHandler("onClientRender", getRootElement(), function()
	if not stuntTextMode or stuntText == nil then return end
	dxDrawBorderedText(2, stuntText, 0, 0, screenX, screenY*0.98, tocolor (240, 240, 240, 255), 1, "bankgothic", "center", "bottom", true, false)
end )

addEventHandler("onClientPlayerStuntFinish", getRootElement(), function(stuntType, stuntTime, stuntDistance)
	--outputChatBox(stuntType.. ": " ..stuntTime.. " " ..stuntDistance)
	if stuntTime < 5000 or stuntDistance < 3 or stuntTextMode or stuntDistance > 20000 then return end 
	
	local stuntCategory
	stuntTextMode = true
	setTimer(function() stuntTextMode = false end, 7000, 1)
	
	givePlayerMoney((stuntTime/1000)*stuntDistance/5)
	
	if stuntType == "stoppie" then
		stuntText = "STOPPIE BONUS: $" ..math.floor((stuntTime/1000)*stuntDistance/5).. " Distance: " ..math.floor(stuntDistance).. "m \nTime: " ..math.floor(stuntTime/1000).. " seconds"
		stuntCategory = 2
	elseif stuntType == "wheelie" then
		stuntText = "WHEELIE BONUS: $" ..math.floor((stuntTime/1000)*stuntDistance/5).. " Distance: " ..math.floor(stuntDistance).. "m \nTime: " ..math.floor(stuntTime/1000).. " seconds"
		stuntCategory = 3
	elseif stuntType == "2wheeler" then
		stuntText = "TWO WHEELS DOUBLE BONUS: $" ..math.floor((stuntTime/1000)*stuntDistance/5).. "\nDistance: " ..math.floor(stuntDistance).. "m Time: " ..math.floor(stuntTime/1000).. " seconds"
		stuntCategory = 4
	end
	
	local stuntData = {}
	stuntData[1] = stuntDistance
	stuntData[2] = stuntTime / 1000
	triggerServerEvent("checkStunt", getLocalPlayer(), stuntCategory, stuntData)
end )