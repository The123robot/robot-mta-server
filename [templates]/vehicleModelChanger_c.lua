VEHICLE_ID = 445
TXD_FILE = "vehicle.txd"
DFF_FILE = "vehicle.dff"

addEventHandler('onClientResourceStart', resourceRoot, 
	function() 
		txd = engineLoadTXD ( TXD_FILE )
		engineImportTXD ( txd, VEHICLE_ID ) 
			
		dff = engineLoadDFF ( DFF_FILE, VEHICLE_ID ) 
		engineReplaceModel ( dff, VEHICLE_ID ) 
	end 
)