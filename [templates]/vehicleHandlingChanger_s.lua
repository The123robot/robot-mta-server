updateHandlingsTimer = nil

-- Function that updates vehicle's handlings
function updateHandlings()
	-- Process every player on the server
	for index, players in ipairs(getElementsByType("player")) do
		-- Get player's vehicle
		local vehicle = getPedOccupiedVehicle(players)
		
		-- If player's vehicle exists then continue
		if vehicle then
			
			-- Get vehicle's model ID
			local vehicleModel = getElementModel(vehicle)
			
			-- Set handling for first model (change YOUR_MODEL_1 for your vehicle's ID)
			if vehicleModel == YOUR_MODEL_1 then
				
				-- set handlings that you want
				setVehicleHandling(vehicle, "mass", 1400.0)
				setVehicleHandling(vehicle, "turnMass", 3000)
				setVehicleHandling(vehicle, "dragCoeff", 2.0 )
				setVehicleHandling(vehicle, "centerOfMass", { 0.0,0.25,-0.30 } )
				setVehicleHandling(vehicle, "percentSubmerged", 50)
				setVehicleHandling(vehicle, "tractionMultiplier", 0.70)
				setVehicleHandling(vehicle, "tractionLoss", 0.8)
				setVehicleHandling(vehicle, "tractionBias", 0.52)
				setVehicleHandling(vehicle, "numberOfGears", 5)
				setVehicleHandling(vehicle, "maxVelocity", 200.0)
				setVehicleHandling(vehicle, "engineAcceleration", 12.0 )
				setVehicleHandling(vehicle, "engineInertia", 10.0)
				setVehicleHandling(vehicle, "driveType", "awd")
				setVehicleHandling(vehicle, "engineType", "petrol")
				setVehicleHandling(vehicle, "brakeDeceleration", 11.0)
				setVehicleHandling(vehicle, "brakeBias", 0.45)
				setVehicleHandling(vehicle, "steeringLock", 30.0 )
				setVehicleHandling(vehicle, "suspensionForceLevel", 2.60)
				setVehicleHandling(vehicle, "suspensionDamping", 0.10 )
				setVehicleHandling(vehicle, "suspensionHighSpeedDamping", 0.0)
				setVehicleHandling(vehicle, "suspensionUpperLimit", 0.28 )
				setVehicleHandling(vehicle, "suspensionLowerLimit", -0.14)
				setVehicleHandling(vehicle, "suspensionFrontRearBias", 0.5 )
				setVehicleHandling(vehicle, "suspensionAntiDiveMultiplier", 0.4)
				setVehicleHandling(vehicle, "seatOffsetDistance", 0.25)
				setVehicleHandling(vehicle, "collisionDamageMultiplier", 0.7)
				setVehicleHandling(vehicle, "modelFlags", 0x40000000)
				setVehicleHandling(vehicle, "handlingFlags", 0x00000001)
				
			elseif vehicleModel == YOUR_MODEL_2 then
				-- your setVehicleHandling for second vehicle model
				
			--elseif vehicleModel == YOUR_MODEL_N then
				-- and so on
			end
		end
	end
end

addEventHandler("onResourceStart", resourceRoot, function()
	-- create a timer for updating handlings
	if isTimer(updateHandlingsTimer) then killTimer(updateHandlingsTimer) end
	updateHandlingsTimer = setTimer(updateHandlings, 20, 0)
end )