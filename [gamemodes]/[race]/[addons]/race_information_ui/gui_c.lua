bigGUI = false
travelScreenDuration = 3000 + 2950

--DEBUG_WIDTH = 800
--DEBUG_HEIGHT = 600

function bindKeys()
	local keys = getBoundKeys("Toggle scoreboard")
	if (not keys) then
		keys = {["F1"] = "down"}
	end
	for key, state in pairs(keys) do
		bindKey(key, "both", toggleBigGUI)
	end
end

function toggleBigGUI(key, state)
	bigGUI = state == "down"
	if (bigGUI) then
		showGUIComponents('bigInfoLabelMap', 'bigInfoLabelFile', 'bigInfoLabelAuthor', 'bigInfoLabelType', 'bigInfoLabelVW', 'bigInfoLabelGM', 'bigInfoLabelRS', 'bigInfoLabelOther', 'bigInfoContentMap', 'bigInfoContentFile', 'bigInfoContentAuthor', 'bigInfoContentType', 'bigInfoContentVW', 'bigInfoContentGM', 'bigInfoContentRS', 'bigInfoContentOther', 'bigInfoContentPing', 'bigInfoLabelPing', 'bigInfoContentFPS', 'bigInfoLabelFPS', 'bigInfoLabelDescription', 'bigInfoContentDescription', 'bigInfoLabelSpawns', 'bigInfoLabelChecks', 'bigInfoLabelObjects', 'bigInfoLabelPickups', 'bigInfoContentSpawns', 'bigInfoContentChecks', 'bigInfoContentObjects', 'bigInfoContentPickups')
		hideGUIComponents('fileDisplay', 'fileDisplayName', 'mapDisplay', 'mapDisplayName', 'authorDisplay', 'authorDisplayName', 'fpsDisplay', 'fpsDisplayCount', 'pingDisplay', 'pingDisplayCount', 'confDisplay', 'confGDisplay', 'confRDisplay', 'confWDisplay')
	else
		showGUIComponents('fileDisplay', 'fileDisplayName', 'mapDisplay', 'mapDisplayName', 'authorDisplay', 'authorDisplayName', 'fpsDisplay', 'fpsDisplayCount', 'pingDisplay', 'pingDisplayCount', 'confDisplay', 'confGDisplay', 'confRDisplay', 'confWDisplay')
		hideGUIComponents('bigInfoLabelMap', 'bigInfoLabelFile', 'bigInfoLabelAuthor', 'bigInfoLabelType', 'bigInfoLabelVW', 'bigInfoLabelGM', 'bigInfoLabelRS', 'bigInfoLabelOther', 'bigInfoContentMap', 'bigInfoContentFile', 'bigInfoContentAuthor', 'bigInfoContentType', 'bigInfoContentVW', 'bigInfoContentGM', 'bigInfoContentRS', 'bigInfoContentOther', 'bigInfoContentPing', 'bigInfoLabelPing', 'bigInfoContentFPS', 'bigInfoLabelFPS', 'bigInfoLabelDescription', 'bigInfoContentDescription', 'bigInfoLabelSpawns', 'bigInfoLabelChecks', 'bigInfoLabelObjects', 'bigInfoLabelPickups', 'bigInfoContentSpawns', 'bigInfoContentChecks', 'bigInfoContentObjects', 'bigInfoContentPickups')
	end
end

function buildGUI()
	local screenWidth, screenHeight = guiGetScreenSize()
	screenWidth = DEBUG_WIDTH or screenWidth
	screenHeight = DEBUG_HEIGHT or screenHeight
	bigUiFont = screenHeight/750
	vertiStart = screenHeight/900*275
	vertiOffset = bigUiFont * 18
	horiOffset = bigUiFont * 78
	g_dxGUI = {
		bigInfoLabelMap = dxText:create('Map Name:', 6, vertiStart, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelFile = dxText:create('Filename:', 6, vertiStart + vertiOffset, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelAuthor = dxText:create('Author:', 6, vertiStart + vertiOffset * 2, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelType = dxText:create('Type:', 6, vertiStart + vertiOffset * 3, false, 'default-bold', bigUiFont, 'left'),

		bigInfoLabelVW = dxText:create('Vehicle Weapons:', 6, vertiStart + vertiOffset * 5, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelGM = dxText:create('Ghost Mode:', 6, vertiStart + vertiOffset * 6, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelRS = dxText:create('Respawn:', 6, vertiStart + vertiOffset * 7, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelOther = dxText:create('Other:', 6, vertiStart + vertiOffset * 8, false, 'default-bold', bigUiFont, 'left'),

		bigInfoLabelSpawns = dxText:create('Spawnpoints:', 6, vertiStart + vertiOffset * 10, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelChecks = dxText:create('Checkpoints:', 6, vertiStart + vertiOffset * 11, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelObjects = dxText:create('Objects:', 6, vertiStart + vertiOffset * 12, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelPickups = dxText:create('Pickups:', 6, vertiStart + vertiOffset * 13, false, 'default-bold', bigUiFont, 'left'),

		bigInfoLabelPing = dxText:create('Ping:', 6, vertiStart + vertiOffset * 15, false, 'default-bold', bigUiFont, 'left'),
		bigInfoLabelFPS = dxText:create('FPS:', 6, vertiStart + vertiOffset * 16, false, 'default-bold', bigUiFont, 'left'),
		

		bigInfoContentMap = dxText:create('UNKNOWN', horiOffset, vertiStart, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentFile = dxText:create('UNKNOWN', horiOffset, vertiStart + vertiOffset, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentAuthor = dxText:create('UNKNOWN', horiOffset, vertiStart + vertiOffset * 2, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentType = dxText:create('UNKNOWN', horiOffset, vertiStart + vertiOffset * 3, false, 'default-bold', bigUiFont, 'left'),

		bigInfoContentVW = dxText:create('UNKNOWN', horiOffset * 1.48, vertiStart + vertiOffset * 5, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentGM = dxText:create('UNKNOWN', horiOffset * 1.48, vertiStart + vertiOffset * 6, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentRS = dxText:create('UNKNOWN', horiOffset * 1.48, vertiStart + vertiOffset * 7, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentOther = dxText:create('UNKNOWN', horiOffset * 1.48, vertiStart + vertiOffset * 8, false, 'default-bold', bigUiFont, 'left'),

		bigInfoContentSpawns = dxText:create('UNKNOWN', horiOffset * 1.15, vertiStart + vertiOffset * 10, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentChecks = dxText:create('UNKNOWN', horiOffset * 1.15, vertiStart + vertiOffset * 11, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentObjects = dxText:create('UNKNOWN', horiOffset * 1.15, vertiStart + vertiOffset * 12, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentPickups = dxText:create('UNKNOWN', horiOffset * 1.15, vertiStart + vertiOffset * 13, false, 'default-bold', bigUiFont, 'left'),

		bigInfoContentPing = dxText:create('UNKNOWN', horiOffset * 0.5, vertiStart + vertiOffset * 15, false, 'default-bold', bigUiFont, 'left'),
		bigInfoContentFPS = dxText:create('UNKNOWN', horiOffset * 0.5, vertiStart + vertiOffset * 16, false, 'default-bold', bigUiFont, 'left'),


		bigInfoLabelDescription = dxText:create('Map Description:', screenWidth*0.25, screenHeight*0.75, false, 'default-bold', 1, 'left'),
		bigInfoContentDescription = dxText:create('UNKNOWN', screenWidth*0.25, screenHeight*0.75+15, false, 'default-bold', 1, 'left'),

		-- Bottom Left Corner Display
		fileDisplay = dxText:create('File:', 4, screenHeight - 10, false, 'default-bold', 1.2, 'left'),
		fileDisplayName = dxText:create('<unknown>', 45, screenHeight - 10, false, 'default-bold', 1.2, 'left'),

		mapDisplay = dxText:create('Map:', 4, screenHeight - 30, false, 'default-bold', 1.2, 'left'),
		mapDisplayName = dxText:create('<unknown>', 45, screenHeight - 30, false, 'default-bold', 1.2, 'left'),

		authorDisplay = dxText:create('By:', 4, screenHeight - 50, false, 'default-bold', 1.2, 'left'),
		authorDisplayName = dxText:create('<unknown>', 45, screenHeight - 50, false, 'default-bold', 1.2, 'left'),

		fpsDisplay = dxText:create('FPS:', 4, screenHeight - 90, false, 'default-bold', 1.2, 'left'),
		fpsDisplayCount = dxText:create('0', 45, screenHeight - 90, false, 'default-bold', 1.2, 'left'),

		pingDisplay = dxText:create('Ping:', 4, screenHeight - 110, false, 'default-bold', 1.2, 'left'),
		pingDisplayCount = dxText:create('0', 45, screenHeight - 110, false, 'default-bold', 1.2, 'left'),

		confDisplay = dxText:create('Conf:', 4, screenHeight - 70, false, 'default-bold', 1.2, 'left'),
		confGDisplay = dxText:create('Gm', 45, screenHeight - 70, false, 'default-bold', 1.2, 'left'),
		confRDisplay = dxText:create('Rs', 75, screenHeight - 70, false, 'default-bold', 1.2, 'left'),
		confWDisplay = dxText:create('Vw', 99, screenHeight - 70, false, 'default-bold', 1.2, 'left'),

		-- Traveling Screen

		travelTextHeader = dxText:create('Travelling to', screenWidth/2, screenHeight/2-230, false, 'bankgothic', 0.60, 'center' ),
		travelTextName = dxText:create('<mapname>', screenWidth/2, screenHeight/2-170, false, 'bankgothic', 0.70, 'center' ),
		travelTextAuthor = dxText:create('<author>', screenWidth/2, screenHeight/2-140, false, 'bankgothic', 0.70, 'center' ),
		travelTextConfigs = dxText:create('<conf>', screenWidth*0.3, screenHeight/2-60, false, 'bankgothic', 0.70, 'left' ),
		travelTextFacts = dxText:create('<facts>', screenWidth*0.52, screenHeight/2-60, false, 'bankgothic', 0.70, 'left' ),

		travelTextDescription = dxText:create('<description>', screenWidth/2, screenHeight/2-140, false, 'bankgothic', 0.70, 'center' ),
	}
	g_GUI = {
		travelImage = guiCreateStaticImage(screenWidth/2-160, screenHeight/2-280, 320, 25, 'img/R.png', false, nil),
	}
	g_dxGUI['travelTextHeader']:color(240,240,240)
	g_dxGUI['travelTextDescription']:align("center","top")
	g_dxGUI['travelTextDescription']:boundingBox(0.2,0.5,0.8,1,true)
	g_dxGUI['travelTextDescription']:wordWrap(true)

	g_dxGUI['bigInfoContentDescription']:align("left","top")
	g_dxGUI['bigInfoContentDescription']:boundingBox(screenWidth*0.25, screenHeight*0.75+12,screenWidth*0.9,screenHeight,false)

	hideGUIComponents('fileDisplay', 'fileDisplayName', 'mapDisplay', 'mapDisplayName', 'authorDisplay', 'authorDisplayName', 'fpsDisplay', 'fpsDisplayCount', 'pingDisplay', 'pingDisplayCount', 'confDisplay', 'confGDisplay', 'confRDisplay', 'confWDisplay')
	hideGUIComponents('bigInfoLabelMap', 'bigInfoLabelFile', 'bigInfoLabelAuthor', 'bigInfoLabelType', 'bigInfoLabelVW', 'bigInfoLabelGM', 'bigInfoLabelRS', 'bigInfoLabelOther', 'bigInfoContentMap', 'bigInfoContentFile', 'bigInfoContentAuthor', 'bigInfoContentType', 'bigInfoContentVW', 'bigInfoContentGM', 'bigInfoContentRS', 'bigInfoContentOther', 'bigInfoContentPing', 'bigInfoLabelPing', 'bigInfoContentFPS', 'bigInfoLabelFPS', 'bigInfoLabelDescription', 'bigInfoContentDescription', 'bigInfoLabelSpawns', 'bigInfoLabelChecks', 'bigInfoLabelObjects', 'bigInfoLabelPickups', 'bigInfoContentSpawns', 'bigInfoContentChecks', 'bigInfoContentObjects', 'bigInfoContentPickups')
	hideGUIComponents('travelImage', 'travelTextHeader', 'travelTextName', 'travelTextAuthor', 'travelTextConfigs', 'travelTextFacts', 'travelTextDescription')
end

addEventHandler('onClientResourceStart', resourceRoot,
	function()
		bindKeys()
		buildGUI()
	end
)

-- -----------
-- Map changed
-- -----------

function showTravelingScreen()
	if (
		g_MapInfo.racetype == "R" or
		g_MapInfo.racetype == "SR" or
		g_MapInfo.racetype == "DD" or
		g_MapInfo.racetype == "TW" or
		g_MapInfo.racetype == "MG"
	) then
		guiStaticImageLoadImage(g_GUI['travelImage'], "img/" .. g_MapInfo.racetype .. ".png")
	else
		guiStaticImageLoadImage(g_GUI['travelImage'], "img/R.png")
	end

	local typeTag = "["..g_MapInfo.racetype.."] "
	g_dxGUI['travelTextName']:text(typeTag .. (g_MapInfo.name or g_MapInfo.resname))
	g_dxGUI['travelTextAuthor']:text(g_MapInfo.author and "Author: " .. g_MapInfo.author or "")
	g_dxGUI['travelTextDescription']:text(g_MapInfo.description or "")
	showGUIComponents('travelImage', 'travelTextHeader', 'travelTextName', 'travelTextAuthor', 'travelTextConfigs', 'travelTextFacts', 'travelTextDescription')
	setTimer(hideTravelingScreen, travelScreenDuration, 1)

	-- Details
	local text5 = ""
	text5 = text5 .. (g_MapInfo.ghostmode and "◼ Ghost mode\n" or "◻ Ghost mode\n")
	text5 = text5 .. (g_MapInfo.respawn == 'timelimit' and "◼ Respawn: ".. g_MapInfo.respawntime/1000 .." sec\n" or "◻ Respawn\n")
	text5 = text5 .. (g_MapInfo.vehicleweapons and "◼ Vehicle weapons\n" or "◻ Vehicle weapons\n")

	local text6 = ""
	text6 = text6 .. g_MapInfo.numspawnpoints .. " Spawnpoint" .. (g_MapInfo.numspawnpoints ~= 1 and "s" or "") .. "\n"
	text6 = text6 .. g_MapInfo.numcheckpoints .. " Checkpoint" .. (g_MapInfo.numcheckpoints ~= 1 and "s" or "") .. "\n"
	text6 = text6 .. g_MapInfo.numobjects .. " Object" .. (g_MapInfo.numobjects ~= 1 and "s" or "") .. "\n"
	text6 = text6 .. g_MapInfo.numpickups .. " Pickup" .. (g_MapInfo.numpickups ~= 1 and "s" or "") .. "\n"
	-- text5 = text5 .. (allowonfoot and "◼ Exiting vehicles\n" or "◻ Exiting vehicles\n")
	-- if (allowonfoot) then
	-- 	text6 = text6 .. (falloffbike and "◼ Fall off bikes\n" or "◻ Fall off bikes\n")
	-- 	text6 = text6 .. (movementglitches and "◼ Super sprint\n" or "◻ Super sprint\n")
	-- 	text6 = text6 .. (fistfights and "◼ Fighting\n" or "◻ Fighting\n")
	-- 	text6 = text6 .. " \n"
	-- end
	g_dxGUI['travelTextConfigs']:text(text5)
	g_dxGUI['travelTextFacts']:text(text6)
end

function hideTravelingScreen()
	hideGUIComponents('travelImage', 'travelTextHeader', 'travelTextName', 'travelTextAuthor', 'travelTextConfigs', 'travelTextFacts', 'travelTextDescription')
end

function updateBottomLeftInformation()
	local typeTag = "["..(g_MapInfo.racetype or "???").."] "
	g_dxGUI.mapDisplayName:text(typeTag .. g_MapInfo.name or "<none>")
	g_dxGUI.fileDisplayName:text(g_MapInfo.resname or "<none>")
	g_dxGUI.authorDisplayName:text(g_MapInfo.author or "<none>")

	g_dxGUI.bigInfoContentMap:text(g_MapInfo.name or "<none>")
	g_dxGUI.bigInfoContentAuthor:text(g_MapInfo.author or "<none>")
	g_dxGUI.bigInfoContentFile:text(g_MapInfo.resname or "<none>")

	g_dxGUI.bigInfoContentSpawns:text(tostring(g_MapInfo.numspawnpoints) or "0")
	g_dxGUI.bigInfoContentChecks:text(tostring(g_MapInfo.numcheckpoints) or "0")
	g_dxGUI.bigInfoContentObjects:text(tostring(g_MapInfo.numobjects) or "0")
	g_dxGUI.bigInfoContentPickups:text(tostring(g_MapInfo.numpickups) or "0")
	if (g_MapInfo.numspawnpoints > 0) then g_dxGUI.bigInfoContentSpawns:color(255,255,255,255) else g_dxGUI.bigInfoContentSpawns:color(127,127,127,255) end
	if (g_MapInfo.numcheckpoints > 0) then g_dxGUI.bigInfoContentChecks:color(255,255,255,255) else g_dxGUI.bigInfoContentChecks:color(127,127,127,255) end
	if (g_MapInfo.numobjects > 0) then g_dxGUI.bigInfoContentObjects:color(255,255,255,255) else g_dxGUI.bigInfoContentObjects:color(127,127,127,255) end
	if (g_MapInfo.numpickups > 0) then g_dxGUI.bigInfoContentPickups:color(255,255,255,255) else g_dxGUI.bigInfoContentPickups:color(127,127,127,255) end

	g_dxGUI.bigInfoContentDescription:text(g_MapInfo.description or "<none>")

	if (g_MapInfo.racetype == "R") then
		g_dxGUI.bigInfoContentType:text("Race (R)")
	elseif (g_MapInfo.racetype == "SR") then
		g_dxGUI.bigInfoContentType:text("Speedrun (SR)")
	elseif (g_MapInfo.racetype == "DD") then
		g_dxGUI.bigInfoContentType:text("Destruction Derby (DD)")
	elseif (g_MapInfo.racetype == "TW") then
		g_dxGUI.bigInfoContentType:text("Teamwork (TW)")
	elseif (g_MapInfo.racetype == "MG") then
		g_dxGUI.bigInfoContentType:text("Minigame (MG)")
	elseif (g_MapInfo.racetype == "F") then
		g_dxGUI.bigInfoContentType:text("Footrace (F)")
	else
		g_dxGUI.bigInfoContentType:text("Unknown ("..(g_MapInfo.racetype or "???")..")")
	end

	if (g_MapInfo.ghostmode) then
		g_dxGUI.confGDisplay:color(255,255,255,255)
		g_dxGUI.bigInfoContentGM:text("Enabled")
		g_dxGUI.bigInfoContentGM:color(255,255,255,255)
	else
		g_dxGUI.confGDisplay:color(127,127,127,255)
		g_dxGUI.bigInfoContentGM:text("Disabled")
		g_dxGUI.bigInfoContentGM:color(127,127,127,255)
	end
	if (g_MapInfo.respawn == "timelimit") then
		g_dxGUI.confRDisplay:color(255,255,255,255)
		g_dxGUI.bigInfoContentRS:text(g_MapInfo.respawntime / 1000 .. " second" .. (g_MapInfo.respawntime == 1000 and "" or "s"))
		g_dxGUI.bigInfoContentRS:color(255,255,255,255)
	else
		g_dxGUI.confRDisplay:color(127,127,127,255)
		g_dxGUI.bigInfoContentRS:text("None")
		g_dxGUI.bigInfoContentRS:color(127,127,127,255)
	end
	if (g_MapInfo.vehicleweapons) then
		g_dxGUI.confWDisplay:color(255,255,255,255)
		g_dxGUI.bigInfoContentVW:text(g_MapInfo.hunterminigun and "Enabled" or "No Hunter Minigun")
		g_dxGUI.bigInfoContentVW:color(255,255,255,255)
	else
		g_dxGUI.confWDisplay:color(127,127,127,255)
		g_dxGUI.bigInfoContentVW:text("Disabled")
		g_dxGUI.bigInfoContentVW:color(127,127,127,255)
	end

	local otherThings = {}
	if (getResourceFromName("extraairresistance")) then
		table.insert(otherThings, "No Air Resistance")
	end
	if (getResourceFromName("randomfoliage")) then
		table.insert(otherThings, "No Random Foliage")
	end
	if (getResourceFromName("poleremover")) then
		table.insert(otherThings, "No Light Poles")
	end
	if (getResourceFromName("bomb")) then
		table.insert(otherThings, "Bomb")
	end
	local otherThingsString = table.concat(otherThings, ", ")
	if (#otherThings > 0) then
		g_dxGUI.bigInfoLabelOther:text("Other:")
		g_dxGUI.bigInfoContentOther:text(otherThingsString)
		g_dxGUI.bigInfoContentOther:color(255,255,255,255)
	else
		g_dxGUI.bigInfoLabelOther:text("")
		g_dxGUI.bigInfoContentOther:text("")
	end
end

addEvent("onMapChangedC", true)
addEventHandler("onMapChangedC", root,
	function(mapInfo)
		g_MapInfo = mapInfo
		
		updateBottomLeftInformation()
		showTravelingScreen()
	end
)

-- ---------------------------------------------------------
-- Visuals (Colors, etc. Requires settings to be read first)
-- ---------------------------------------------------------

function updateGUIDisplay()
	local r,g,b,a = getColorFromString(g_Options.colorbottomleft)
	g_dxGUI.fileDisplay:color(r,g,b,a)
	g_dxGUI.fileDisplay:type('stroke', 1.5)
	g_dxGUI.fileDisplayName:color(255, 255, 255, 255)
	g_dxGUI.fileDisplayName:type('stroke', 1.5)

	g_dxGUI.mapDisplay:color(r,g,b,a)
	g_dxGUI.mapDisplay:type('stroke', 1.5)
	g_dxGUI.mapDisplayName:color(255, 255, 255, 255)
	g_dxGUI.mapDisplayName:type('stroke', 1.5)

	g_dxGUI.authorDisplay:color(r,g,b,a)
	g_dxGUI.authorDisplay:type('stroke', 1.5)
	g_dxGUI.authorDisplayName:color(255, 255, 255, 255)
	g_dxGUI.authorDisplayName:type('stroke', 1.5)

	g_dxGUI.fpsDisplay:color(r,g,b,a)
	g_dxGUI.fpsDisplay:type('stroke', 1.5)
	g_dxGUI.fpsDisplayCount:color(255, 255, 255, 255)
	g_dxGUI.fpsDisplayCount:type('stroke', 1.5)

	g_dxGUI.pingDisplay:color(r,g,b,a)
	g_dxGUI.pingDisplay:type('stroke', 1.5)
	g_dxGUI.pingDisplayCount:color(255, 255, 255, 255)
	g_dxGUI.pingDisplayCount:type('stroke', 1.5)

	g_dxGUI.confDisplay:color(r,g,b,a)
	g_dxGUI.confGDisplay:color(127, 127, 127, 255)
	g_dxGUI.confRDisplay:color(127, 127, 127, 255)
	g_dxGUI.confWDisplay:color(127, 127, 127, 255)
	g_dxGUI.confDisplay:type('stroke', 1.5)
	g_dxGUI.confGDisplay:type('stroke', 1.5)
	g_dxGUI.confRDisplay:type('stroke', 1.5)
	g_dxGUI.confWDisplay:type('stroke', 1.5)

	-- big info

	g_dxGUI.bigInfoLabelMap:color(r,g,b,a)
	g_dxGUI.bigInfoLabelMap:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelAuthor:color(r,g,b,a)
	g_dxGUI.bigInfoLabelAuthor:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelFile:color(r,g,b,a)
	g_dxGUI.bigInfoLabelFile:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelType:color(r,g,b,a)
	g_dxGUI.bigInfoLabelType:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelGM:color(r,g,b,a)
	g_dxGUI.bigInfoLabelGM:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelRS:color(r,g,b,a)
	g_dxGUI.bigInfoLabelRS:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelVW:color(r,g,b,a)
	g_dxGUI.bigInfoLabelVW:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelOther:color(r,g,b,a)
	g_dxGUI.bigInfoLabelOther:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelPing:color(r,g,b,a)
	g_dxGUI.bigInfoLabelPing:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelFPS:color(r,g,b,a)
	g_dxGUI.bigInfoLabelFPS:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelDescription:color(r,g,b,a)
	g_dxGUI.bigInfoLabelDescription:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelSpawns:color(r,g,b,a)
	g_dxGUI.bigInfoLabelSpawns:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelChecks:color(r,g,b,a)
	g_dxGUI.bigInfoLabelChecks:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelObjects:color(r,g,b,a)
	g_dxGUI.bigInfoLabelObjects:type('stroke', 1.7)
	g_dxGUI.bigInfoLabelPickups:color(r,g,b,a)
	g_dxGUI.bigInfoLabelPickups:type('stroke', 1.7)

	g_dxGUI.bigInfoContentMap:type('stroke', 1.7)
	g_dxGUI.bigInfoContentAuthor:type('stroke', 1.7)
	g_dxGUI.bigInfoContentFile:type('stroke', 1.7)
	g_dxGUI.bigInfoContentType:type('stroke', 1.7)
	g_dxGUI.bigInfoContentGM:type('stroke', 1.7)
	g_dxGUI.bigInfoContentRS:type('stroke', 1.7)
	g_dxGUI.bigInfoContentVW:type('stroke', 1.7)
	g_dxGUI.bigInfoContentOther:type('stroke', 1.7)
	g_dxGUI.bigInfoContentFPS:type('stroke', 1.7)
	g_dxGUI.bigInfoContentPing:type('stroke', 1.7)
	g_dxGUI.bigInfoContentDescription:type('stroke', 1.7)
	g_dxGUI.bigInfoContentSpawns:type('stroke', 1.7)
	g_dxGUI.bigInfoContentChecks:type('stroke', 1.7)
	g_dxGUI.bigInfoContentObjects:type('stroke', 1.7)
	g_dxGUI.bigInfoContentPickups:type('stroke', 1.7)
end

addEvent('onResourceSettingsReady')
addEventHandler('onResourceSettingsReady', resourceRoot,
	function()
		updateGUIDisplay()
		showGUIComponents('fileDisplay', 'fileDisplayName', 'mapDisplay', 'mapDisplayName', 'authorDisplay', 'authorDisplayName', 'fpsDisplay', 'fpsDisplayCount', 'pingDisplay', 'pingDisplayCount', 'confDisplay', 'confGDisplay', 'confRDisplay', 'confWDisplay')
	end
)

function showGUIComponents(...)
	for i,name in ipairs({...}) do
		if g_dxGUI[name] then
			g_dxGUI[name]:visible(true)
		elseif type(g_GUI[name]) == 'table' then
			g_GUI[name]:show()
		else
			guiSetVisible(g_GUI[name], true)
		end
	end
end

function hideGUIComponents(...)
	for i,name in ipairs({...}) do
		if g_dxGUI[name] then
			g_dxGUI[name]:visible(false)
		elseif type(g_GUI[name]) == 'table' then
			g_GUI[name]:hide()
		else
			guiSetVisible(g_GUI[name], false)
		end
	end
end

-- ------------
-- FPS and Ping
-- ------------
local fpsString = 0
addEventHandler('onClientPreRender', root,
	function (msSinceLastFrame)
		if (DEBUG_WIDTH and DEBUG_HEIGHT) then
			dxDrawRectangle(0, 0, DEBUG_WIDTH, DEBUG_HEIGHT, tocolor("#0000FF4A"))
		end
		fpsString = (1 / msSinceLastFrame) * 1000
	end
)

function updateFPSAndPing()
	local ping = tostring(getPlayerPing(localPlayer))
	local fpsLimit = getFPSLimit()
	local fpsString = tostring(math.floor(fpsString * 10) / 10)
	local fpsStringBigInfo = tostring(math.floor(fpsString * 10) / 10) .. " / " .. fpsLimit
	g_dxGUI.bigInfoContentFPS:text(fpsStringBigInfo)
	g_dxGUI.fpsDisplayCount:text(fpsString)
	g_dxGUI.bigInfoContentPing:text(ping)
	g_dxGUI.pingDisplayCount:text(ping)
end
setTimer(updateFPSAndPing, 1000, 0)
