g_mapInfo = {}
g_raceGamemode = getResourceRootElement(getResourceFromName("race"))
g_mapManager = getResourceFromName("mapmanager")
g_mapResource = nil

function mapInfoChanged(theKey, oldValue, newValue)
	if (theKey ~= "info") then return end

	if g_mapManager then
		g_mapResource = exports.mapmanager:getRunningGamemodeMap()
	end
	
	local resourceName = newValue.mapInfo.resname
	local raceData = getRaceGamemodeSettings()

	local gm = getBool(resourceName .. ".ghostmode")
	local vw = getBool(resourceName .. ".vehicleweapons")
	local hm = getBool(resourceName .. ".hunterminigun")
	local rs = getString(resourceName .. ".respawn")
	if gm == nil then gm = raceData.ghostmode end
	if vw == nil then vw = raceData.vehicleweapons end
	if hm == nil then hm = raceData.hunterminigun end
	if rs == nil then rs = raceData.respawnmode end

	g_mapInfo = {
		resname = resourceName,
		name = newValue.mapInfo.name,
		author = newValue.mapInfo.author,
		racetype = getResourceInfo(g_mapResource, "racetype") or getString(g_mapInfo.resname .. ".racetype") or "???",
		description = getResourceInfo(g_mapResource, "description"),

		numspawnpoints = newValue.mapInfo.numspawnpoints,
		numcheckpoints = newValue.mapInfo.numcheckpoints,
		numobjects = newValue.mapInfo.numobjects,
		numpickups = newValue.mapInfo.numpickups,

		ghostmode = ternary(raceData.ghostmode_map_can_override, gm, raceData.ghostmode),
		vehicleweapons = ternary(raceData.vehicleweapons_map_can_override, vw, raceData.vehicleweapons),
		hunterminigun = ternary(raceData.hunterminigun_map_can_override, hm, raceData.hunterminigun),
		respawn = rs,
		respawntime = newValue.mapOptions.respawntime or raceData.respawntime,
	}


	triggerClientEvent(root, "onMapChangedC", resourceRoot, g_mapInfo)
end
addEventHandler("onElementDataChange", g_raceGamemode, mapInfoChanged)

addEventHandler("onResourceStart", resourceRoot,
	function ()
		local mapmanager = getResourceFromName("mapmanager")
		local fetchedData = getElementData(getResourceRootElement(getResourceFromName("race")), "info" )
		local raceData = getRaceGamemodeSettings()

		
		if mapmanager then
			local mapResource = exports.mapmanager:getRunningGamemodeMap()
			if mapResource then
				local mapFile = getResourceName(mapResource)
				local gm = getBool(mapFile .. ".ghostmode")
				local vw = getBool(mapFile .. ".vehicleweapons")
				local hm = getBool(mapFile .. ".hunterminigun")
				local rs = getString(mapFile .. ".respawn")
				if gm == nil then gm = raceData.ghostmode end
				if vw == nil then vw = raceData.vehicleweapons end
				if hm == nil then hm = raceData.hunterminigun end
				if rs == nil then rs = raceData.respawnmode end

				g_mapInfo = {
					resname = mapFile,
					name = getResourceInfo(mapResource, "name"),
					author = getResourceInfo(mapResource, "author"),
					racetype = getResourceInfo(mapResource, "racetype") or getString(mapFile .. ".racetype") or "???",
					description = getResourceInfo(mapResource, "description"),

					numspawnpoints = fetchedData.mapInfo.numspawnpoints,
					numcheckpoints = fetchedData.mapInfo.numcheckpoints,
					numobjects = fetchedData.mapInfo.numobjects,
					numpickups = fetchedData.mapInfo.numpickups,

					ghostmode = ternary(raceData.ghostmode_map_can_override, gm, raceData.ghostmode),
					vehicleweapons = ternary(raceData.vehicleweapons_map_can_override, vw, raceData.vehicleweapons),
					hunterminigun = ternary(raceData.hunterminigun_map_can_override, hm, raceData.hunterminigun),
					respawn = rs,
					respawntime = fetchedData.mapOptions.respawntime or raceData.respawntime,
				}
			end
		end
	end
)

function getRaceGamemodeSettings()
	return {
		ghostmode = getBool("race.ghostmode"),
		ghostmode_map_can_override = getBool("race.ghostmode_map_can_override"),
		vehicleweapons = getBool("race.vehicleweapons"),
		vehicleweapons_map_can_override = getBool("race.vehicleweapons_map_can_override"),
		hunterminigun = getBool("race.hunterminigun"),
		hunterminigun_map_can_override = getBool("race.hunterminigun_map_can_override"),
		respawnmode = getString("race.respawnmode"),
		respawntime = getNumber("race.respawntime"),
	}
end

addEvent("onClientWantMapData", true)
addEventHandler('onClientWantMapData', root,
	function()
		triggerClientEvent(source, "onMapChangedC", resourceRoot, g_mapInfo)
	end
)
