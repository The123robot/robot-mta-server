local pile
local getSmaller = false
local getBigger = false
local startTime
local animationTime = 150
local drawStuff = true

function ClientStart()
	pile = getElementByID("pile")
end
addEventHandler("onClientResourceStart",root,ClientStart)

function raceStarted()
    --setDevelopmentMode(true)
    setTimer(function()
    	drawStuff = false
    end, 5000, 1)
end
addEvent("raceStarted", true)
addEventHandler("raceStarted", root, raceStarted)

function getSmall()
	getSmaller = true
	startTime = getTickCount()
end
addEvent("getSmall", true)
addEventHandler("getSmall", root, getSmall)

function getBig()
	getBigger = true
	startTime = getTickCount()
end
addEvent("getBig", true)
addEventHandler("getBig", root, getBig)

function render()
	if getSmaller then
		local ticks = getTickCount() - startTime

		if ticks <= animationTime then
			setObjectScale(pile, (animationTime - ticks) / animationTime)
		else
			setObjectScale(pile, 0)
			getSmaller = false
		end
	elseif getBigger then
		local ticks = getTickCount() - startTime

		if ticks <= animationTime then
			setObjectScale(pile, ticks / animationTime)
		else
			setObjectScale(pile, 1)
			getBigger = false
		end
	end

	if drawStuff then
		local screenWidth,screenHeight = guiGetScreenSize() 

		local pilePositionX = -1499.0996
		local pilePositionY = -383.90039
		local pilePositionZ = 81.7

		local posX, posY = getScreenFromWorldPosition(pilePositionX, pilePositionY, pilePositionZ + 1)

		if posX == false then
			return
		end

		local cameraPosX, cameraPosY, cameraPosZ = getElementPosition(getCamera())

		local textSize = (screenHeight / 1080) * math.max(10/getDistanceBetweenPoints3D(pilePositionX, pilePositionY,
				pilePositionZ, cameraPosX, cameraPosY, cameraPosZ), 1)

		local text = "The goal is below this pile, it disappears randomly\n     If more than one person goes down they all die"

		width = dxGetTextWidth(text, textSize, "pricedown")

		dxDrawBorderedText (1, text, posX - (width / 2), posY, 1000, 1000, tocolor ( 255, 255, 255, 255 ), textSize, "pricedown")
	end
end
addEventHandler ( "onClientRender", root, render )

function dxDrawBorderedText (outline, text, left, top, right, bottom, color, scale, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    local outline = (scale or 1) * (1.333333333333334 * (outline or 1))
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outline, top - outline, right - outline, bottom - outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outline, top - outline, right + outline, bottom - outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outline, top + outline, right - outline, bottom + outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outline, top + outline, right + outline, bottom + outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outline, top, right - outline, bottom, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outline, top, right + outline, bottom, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left, top - outline, right, bottom - outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left, top + outline, right, bottom + outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text, left, top, right, bottom, color, scale, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
end