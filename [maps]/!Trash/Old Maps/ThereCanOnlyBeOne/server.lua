local pile
local pileStartX, pileStartY, pileStartZ
local zMovement = 40
local holeCol

function ServerStart()
	pile = getElementByID("pile")
	pileStartX, pileStartY, pileStartZ = getElementPosition(pile)

	holeCol = createColTube(pileStartX, pileStartY, pileStartZ - zMovement - 1, 
		9,  zMovement - 2)
end
addEventHandler("onResourceStart",root,ServerStart)

function raceStateChanged(newStateName, oldStateName)
	if newStateName == "Running" and oldStateName == "GridCountdown" then
		triggerClientEvent("raceStarted", root)
		setTimer(randomlyMoveThePile, 3000, 1)
	end
end
addEvent( "onRaceStateChanging", true )
addEventHandler( "onRaceStateChanging", resourceRoot, raceStateChanged )


function randomlyMoveThePile()
	setTimer(moveThePile, math.random(3000, 8000), 1)
end

function moveThePile()
	setElementCollisionsEnabled(pile, false)
	triggerClientEvent("getSmall", root)

	setTimer(function()
		setElementCollisionsEnabled(pile, true)
		triggerClientEvent("getBig", root)

		randomlyMoveThePile()
		--checkHole()
		setTimer(function()
    		checkHole()
    	end, 1000, 1)
	end, 1000, 1)
end

function checkHole()
	playersInHoleCol = getElementsWithinColShape(holeCol, "player")

	if #playersInHoleCol > 1 then
		for _, player in ipairs( playersInHoleCol ) do
    		setElementPosition (getPedOccupiedVehicle(player), 
    			pileStartX + math.random(-5, 5), 
    			pileStartY + math.random(-5, 5), 
    			pileStartZ + 4)
    		setElementVelocity (getPedOccupiedVehicle(player), 
    			math.random(-0.2, 0.2), math.random(-0.2, 0.2), 0.5)
    		--blowVehicle(getPedOccupiedVehicle(player))
    		setTimer(function()
    			blowVehicle(getPedOccupiedVehicle(player))
			end, math.random(0, 250), 1)
    	end
	end
end