local sW,sH = guiGetScreenSize()
local gRoot = getRootElement()
local gMe = getLocalPlayer()

local h = dxGetFontHeight ( 2, "bankgothic" )
local h2 = 2*h
local Y = sH-50
local Y1 = Y-h
local Y2 = Y-h2
local alpha = 0

local offSet = 16.7

local myObjCount = 0

function replaceModel()
	local txd = engineLoadTXD ( "carshade/carshade.txd" )
	local col = engineLoadCOL ( "carshade/carshade.col" )
	local dff = engineLoadDFF ( "carshade/carshade.dff" )
	engineImportTXD ( txd, 1940 )
	engineReplaceCOL ( col, 1940 )
	engineReplaceModel ( dff, 1940 )
	
	local txd = engineLoadTXD ( "textures/rock1.txd" )
	engineImportTXD ( txd, 17034 )
	engineImportTXD ( txd, 17031 )
	local txd = engineLoadTXD ( "textures/rock2.txd" )
	engineImportTXD ( txd, 18228 )
	
	local txd1 = engineLoadTXD ( "textures/luxor.txd" )
	engineImportTXD ( txd1, 8397 )
	
	addEventHandler("onClientRender",gRoot,Rendering)	
	addEventHandler("onClientPreRender",gRoot,Drawings)
	
	createBlip(5000,0,0,27)
	
	triggerServerEvent("onClientReady",gMe)
end
addEventHandler("onClientResourceStart",getResourceRootElement(getThisResource()),replaceModel)

function restoreModel()
	engineRestoreCOL ( 1940 )
	engineRestoreModel ( 1940 )
end
addEventHandler("onClientResourceStop",getResourceRootElement(getThisResource()),restoreModel)

function startDrawing(dT)
	gMeCar = getPedOccupiedVehicle(gMe)
	
	local x,y = getElementPosition(gMeCar)
	local x,y = x-5000,y
	setElementVelocity(gMeCar,x/50,y/50,0.2)
	

	drawTime = dT
	if not tonumber(drawTime) then drawTime = 15 end
	
	startTime = getTickCount()
	
	onStart()
end
addEvent("drawingStart",true)
addEventHandler('drawingStart', gRoot,startDrawing)

function drawingStop(palms)
	palmTreesPlayer = palms
	stopRendering = true
	
	myObjCount = myObjCount + 1
			
	local x,y = getElementPosition(gMeCar)
	
	local r = findRotation(x,y,X,Y)+90
	local rrad = math.rad(r+90)
		
	local pX,pY = X-offSet*math.sin(rrad),Y+offSet*math.cos(rrad)
	myObjCount = myObjCount+1
	triggerServerEvent("createCarShade",gMe,pX,pY,r,myObjCount)
	
	toggleControl ( "accelerate", true )
	toggleControl ( "handbrake", true )
	toggleControl ( "brake_reverse", true )
	setControlState ( "handbrake", false )
	setElementVelocity(gMeCar,0,0,0.2)
	
	setTimer(function()
		local x,y = getElementPosition(gMeCar)
		for i=1,palmTreesPlayer do
			freePlace = true
			local objX,objY = x+math.random(-100,100),y+math.random(-100,100)
			for a=-2,2 do
				for b=-2,2 do
					if not isLineOfSightClear(objX+a,objY+b,2,objX+a,objY+b,1.1,true,true,true,true,true,true,true) then freePlace = false end
				end
			end
			if freePlace then
				triggerServerEvent("createPalm",gMe,objX,objY)
			end
		end
	end,math.random(1000,10000),1)
	
	removeEventHandler("onClientRender",gRoot,CheckingDistance)
	X,Y = nil,nil
	
	destroyElement(ghostShade)
end
addEvent("drawingStop",true)
addEventHandler('drawingStop', gRoot,drawingStop)

function onStart()
	local o = findClosestShade()
	local x,y,z = getElementPosition(o)
	local _,_,r = getElementRotation(o)
	local r = r+180
	local r = -math.rad(r+90)
	X,Y = x+offSet*math.sin(r),y+offSet*math.cos(r)
	addEventHandler("onClientRender",gRoot,CheckingDistance)
	
	ghostShade = createObject(1940,x,y,-2,0,0,90)
end

function CheckingDistance()
	local x,y = getElementPosition(gMe)
	local d = getDistanceBetweenPoints2D(X,Y,x,y)
	local r = findRotation(x,y,X,Y)+90
	local rrad = math.rad(r+90)
		
	local pX,pY = X-offSet*math.sin(rrad),Y+offSet*math.cos(rrad)
	
	setElementPosition(ghostShade,pX,pY,-2)
	setElementRotation(ghostShade,0,0,r)
	
	if d > 40 then
		if pX >= 4500 and pX <= 5500 and pY >= -469.15 and pY <= 469.15 then
			myObjCount = myObjCount+1
			triggerServerEvent("createCarShade",gMe,pX,pY,r,myObjCount)
			local r = -math.rad(r+90)
			X,Y = pX+offSet*math.sin(r),pY+offSet*math.cos(r)
		end
	end
end

function findClosestShade()
	if isElement(gMeCar) then
		local x,y = getElementPosition(gMeCar)
		for i,o in ipairs(getElementsByType("object")) do
			if getElementModel(o) == 1940 then 
				local ox,oy = getElementPosition(o)
				local d = getDistanceBetweenPoints2D(x,y,ox,oy)
				if not cloDist then cloDist = d cloObj = o end
				if d < cloDist then cloDist = d cloObj = o end
			end
		end
		return cloObj
	end
end

Effect = 0
function Rendering()
	if alpha < 255 then alpha = alpha+1 end
	if stopRendering then Text = "FIGHT!" else Text = "Draw a DD map" end
	dxDrawText(Text,2,Y1+2,sW+2,Y1+2,tocolor(0,0,0,alpha),2,"bankgothic","center","center")
	dxDrawText(Text,0,Y1,sW,Y1,tocolor(0,255,255,alpha),2,"bankgothic","center","center")
	if not stopRendering then
		if drawTime then
			local timeLeft = drawTime-math.floor((getTickCount()-startTime)/1000)
			if timeLeft >= 0 then
				dxDrawText(timeLeft,2,Y2+2+SIN*5,sW+2,Y2+2+SIN*5,tocolor(0,0,0,alpha),2,"bankgothic","center","center")
				dxDrawText(timeLeft,0,Y2+SIN*5,sW,Y2+SIN*5,tocolor(0,155,255,alpha),2,"bankgothic","center","center")
				if timeLeft <= 3 then
					if isElement(gMeCar) then
						if isControlEnabled ( "accelerate" ) then
							toggleControl ( "accelerate", false )
							toggleControl ( "handbrake", false )
							toggleControl ( "brake_reverse", false )
							setControlState ( "handbrake", true )
						end
					end	
				end
			end
		end		
	end
end

WaterTexture = dxCreateTexture("colors.png")
LineTexture = dxCreateTexture("line.png")
function Drawings()
	Effect = Effect+0.1
	if Effect > 2*math.pi then Effect = 0 end
	SIN = math.sin(Effect)
	for i,marker in ipairs(getElementsByType("marker")) do
		local x,y,z = getElementPosition(marker)
		if math.mod(i,2) == 0 then
			setElementPosition(marker,x,y,z+SIN/10)
		else
			setElementPosition(marker,x,y,z-SIN/10)
		end
	end
	
	dxDrawMaterialLine3D(5500,-469.15,4,4500,-469.15,4,LineTexture,8,tocolor(200,200,150,128-50*SIN),0,0,2)
	dxDrawMaterialLine3D(5500,469.15,4,4500,469.15,4,LineTexture,8,tocolor(200,200,150,128-50*SIN),0,0,2)
	dxDrawMaterialLine3D(5500,-469.15,4,5500,469.15,4,LineTexture,8,tocolor(200,200,150,128-50*SIN),0,0,2)
	dxDrawMaterialLine3D(4500,-469.15,4,4500,469.15,4,LineTexture,8,tocolor(200,200,150,128-50*SIN),0,0,2)
	
	dxDrawMaterialLine3D (5000,-469.15,0,5000,469.15,0,WaterTexture, 1000, tocolor(255,255,255,100),5000,0,100)
end

---------------------------------------------------------------------------------------
--------------------------- UTILS -----------------------------------------------------
---------------------------------------------------------------------------------------


function findRotation(x1,y1,x2,y2)
 
  local t = -math.deg(math.atan2(x2-x1,y2-y1))
  if t < 0 then t = t + 360 end;
  return t;
 
end