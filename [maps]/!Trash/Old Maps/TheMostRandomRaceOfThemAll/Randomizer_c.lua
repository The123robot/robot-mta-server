-- Randomizer Coaster Resource -- 
-- CLIENTSIDE SCRIPT --
-- This script saves the race progress when hitting a checkpoint

-- function wasted( killer, weapon, bodypart )
	
-- end
-- addEventHandler ( "onClientPlayerWasted", getLocalPlayer(), wasted )
save = true
function MarkerHit ( hitPlayer, matchingDimension )

	if (hitPlayer == localPlayer) and (getPedOccupiedVehicle(hitPlayer)) and (save) then
		x,y,z = getElementPosition(getPedOccupiedVehicle(hitPlayer))
		rotx,roty,rotz = getElementRotation(getPedOccupiedVehicle(hitPlayer))
		-- outputChatBox("Location saved: " .. rotz)
	end
end
addEventHandler ( "onClientMarkerHit", getRootElement(), MarkerHit )



function respawn()
	
	if (x) then -- and (getPedOccupiedVehicle(localPlayer)) then
	save = false	
		theTimer = setTimer ( function()
						if (getPedOccupiedVehicle(localPlayer)) then
							killTimer(theTimer)
							setElementPosition(getPedOccupiedVehicle(localPlayer),x,y,z)
							setElementRotation(getPedOccupiedVehicle(localPlayer),0,0,rotz)
							save = true
						end
		end, 50, 0 )
		
	end
end
addEventHandler ( "onClientPlayerSpawn", getLocalPlayer(),respawn)