-- Randomizer Coaster Resource -- 
-- SERVERSIDE SCRIPT --
-- This script makes a random track consisting of a curve, helix, looping and straight section to race on --

-- Various counters
elementCounter = 0
cornerCounter = -1
heightCounter = 0

-- Tracks number of consecutive turns
maxTurns = 2
consecutiveTurns = 0
edgeTurns = 0
turnCooldown = 0
ignoreEdgeCooldown = 0

-- function to create a random 'element' (section) of the track
function createRandomElement(counter,startelement)
	local x,y,z = getElementPosition(startelement)
	local rotx,roty,rotz = getElementRotation(startelement)
		-- general extension before curve
	local refdirx = 80*math.cos(math.rad(rotz))
	local refdiry = 80*math.sin(math.rad(rotz))
	createObject(18450,x+refdirx,y+refdiry,z,0,0,rotz)
	
	local edgeCheck = false
	
	if (((x + 200) > 3000 or (x - 200) < -3000 or (y + 200) > 3000 or (y - 200) < -3000) and turnCooldown < 1 and ignoreEdgeCooldown < 1) then
		edgeCheck = true
		consecutiveTurns = consecutiveTurns + 1
		edgeTurns = edgeTurns + 1
		cornerCounter = 1
	end
	
	if (ignoreEdgeCooldown > 0) then
		ignoreEdgeCooldown = ignoreEdgeCooldown - 1
	end
	
	if (edgeTurns == 2) then
		ignoreEdgeCooldown = 4
	end

	-- General Curve (left or right)
	if (counter == 1 or edgeCheck) then
		createMarker(x+refdirx,y+refdiry,z,"checkpoint",8)
			
		-- reference points for the first x-twist
		local refx = x+2*refdirx
		local refy = y+2*refdiry
		reference1 = createObject(18450,refx,refy,z,0,0,rotz) -- reference objects of the first x-twist
	
		if (cornerCounter == -1) then
			-- parameters for curve right
			cornerCounter = cornerCounter + 1
			Rotation1 = 30
			ZRotation = -90
			xcurve = 40
			ycurve = -40
		elseif (cornerCounter == 1) then
			-- parameters for curve left
			cornerCounter = cornerCounter - 1
			Rotation1 = -30
			ZRotation = 90
			xcurve = 40
			ycurve = -40
		elseif (cornerCounter == 0) then
			-- random left or right
			if (math.random(0,1) <= 0.5) then
			cornerCounter = cornerCounter + 1
			Rotation1 = 30
			ZRotation = -90
			xcurve = 40
			ycurve = -40
			else
			cornerCounter = cornerCounter - 1
			Rotation1 = -30
			ZRotation = 90
			xcurve = 40
			ycurve = 40
			end
		end
	
		for i=0,1,1/30 do -- drawing the first twisting section
			local xoffset = i*refdirx
			local yoffset = i*refdiry
			createObject(18450,refx+xoffset,refy+yoffset,z,i*Rotation1,0,rotz)
		end
		local reference2 = createObject(18450,x+4*refdirx,y+4*refdiry,z,Rotation1,0,rotz) -- reference object for the start of the turn
		for i=0,1,1/30 do -- drawning the curve
			local xoffset =i*(xcurve)
			local yoffset =i*(ycurve)
			createObject(18450,(x+4*refdirx)+xoffset,(y+4*refdiry)+yoffset,z,Rotation1,0,rotz+i*ZRotation)
		end
		local reference3 = createObject(18450,x+4*refdirx+xcurve,y+4*refdiry+ycurve,z,Rotation1,0,rotz+ZRotation) -- end of the turn
		
	
		local newx = x+4*refdirx+xcurve
		local newy = y+4*refdiry+ycurve
		
		local refdirx2 = 80*math.cos(math.rad(rotz+ZRotation))
		local refdiry2 = 80*math.sin(math.rad(rotz+ZRotation))
		
		local reference4 = createObject(18450,newx+refdirx2,newy+refdiry2,z,Rotation1,0,rotz+ZRotation)
		for i=0,1,1/30 do -- drawing the second twisting section
				local xoffset = i*refdirx2+refdirx2
				local yoffset = i*refdiry2+refdiry2
				createObject(18450,newx+xoffset,newy+yoffset,z,Rotation1-i*Rotation1,0,rotz+ZRotation)
		end
		local reference5 = createObject(18450,newx+2*refdirx2,newy+2*refdiry2,z,0,0,rotz+ZRotation)
		return reference5
	end

-- Looping
	if (counter == 2) then
		createMarker(x+refdirx,y+refdiry,z,"corona",8,0,255,0) -- create special marker for nitro
		createObject(18450,x+2*refdirx,y+2*refdiry,z,0,0,rotz)
		createObject(18450,x+3*refdirx,y+3*refdiry,z,0,0,rotz)
		createObject(18450,x+4*refdirx,y+4*refdiry,z,0,0,rotz)
		createObject(18450,x+5*refdirx,y+5*refdiry,z,0,0,rotz)
		-- parameters for center of looping
		local x1 = x+5*refdirx
		local y1 = y+5*refdiry
		local zcenter = z+40
		
		if (math.random(0,1) <= 0.5) then -- random for left or right
			direction = 1
		else
			direction = -1
		end
		
			for i=0,1,1/200 do
				local zoffset = - math.cos(math.rad(i*360))*40
				local xoffset = math.cos(math.rad(rotz))*40*math.sin(math.rad(i*360)) - direction*math.sin(math.rad(rotz))*i*40
				local yoffset = math.sin(math.rad(rotz))*40*math.sin(math.rad(i*360)) + direction*math.cos(math.rad(rotz))*i*40

				createObject(18450,x1+xoffset,y1+yoffset,zcenter+zoffset,0,-i*360,rotz)	
			end
		local reference2 = createObject(18450,x1 - direction*math.sin(math.rad(rotz))*40,y1+ direction*math.cos(math.rad(rotz))*40,z,0,0,rotz)
		return reference2
	end
	
-- Helix up/down  left/right
	--[[if (counter == 2) then
		createMarker(x+refdirx,y+refdiry,z,"checkpoint",8)
		if (math.random(0,1) < 0.5) then -- rechts
			Rotation2 = 30
			clockwise = 1
			
			if (cornerCounter == -1) then
			helixcenterx = 80
			angleOffset = 180
			elseif (cornerCounter == 0) then
			helixcenterx = 0
			angleOffset = 90
			elseif (cornerCounter == 1) then
			helixcenterx = -80
			angleOffset = 0
			end
			
		else  -- links
			Rotation2 = -30
			clockwise = -1
			
			if (cornerCounter == -1) then
			helixcenterx = -80
			angleOffset = 0
			elseif (cornerCounter == 0) then
			helixcenterx = 0
			angleOffset = -90
			elseif (cornerCounter == 1) then
			helixcenterx = 80
			angleOffset = 180
			end
		end
		local helixcentery = clockwise * (math.abs(cornerCounter)-1) * 80
		
		if (heightCounter ~= 0) and (math.random(0,1) < 0.5) then
			height = -1
			heightCounter = heightCounter - 1
		else
			height = 1
			heightCounter = heightCounter + 1
		end
		
		local refx = x+2*refdirx
		local refy = y+2*refdiry
		reference1 = createObject(18450,refx,refy,z,0,0,rotz) -- reference objects of the first x-twist
		for i=0,1,1/30 do -- drawing the first twisting section
			local xoffset = i*refdirx
			local yoffset = i*refdiry
			createObject(18450,refx+xoffset,refy+yoffset,z,i*Rotation2,0,rotz)
		end
		local reference2 = createObject(18450,x+4*refdirx,y+4*refdiry,z,Rotation2,0,rotz) -- reference object for the start of the helix
	
		-- outputChatBox("helixstart y:" .. y+4*refdiry)
		-- outputChatBox("helixcenter y:" .. y+4*refdiry+helixcentery)
	
		for i=0,1,1/200 do
				local zoffset = 20*i*height
				local xoffset = math.cos(-clockwise*math.rad(i*360)+math.rad(angleOffset))*80          --math.cos(-clockwise*math.rad(i*360)+angleOffset)*80
				local yoffset = math.sin(-clockwise*math.rad(i*360)+math.rad(angleOffset))*80

				createObject(18450,(x+4*refdirx)+helixcenterx+xoffset,y+4*refdiry+helixcentery + yoffset,z+zoffset,Rotation2,0,rotz-clockwise*i*360)
				--outputChatBox("helix step y:" .. y+4*refdiry+helixcentery + yoffset)				
		end
		local reference3 = createObject(18450,x+4*refdirx,y+4*refdiry,z+20*height,Rotation2,0,rotz) -- reference object for the end of the helix
		local reference4 = createObject(18450,x+5*refdirx,y+5*refdiry,z+20*height,Rotation2,0,rotz) -- reference object for the start of the twist
		
		for i=0,1,1/30 do -- drawing the first twisting section
			local xoffset = i*refdirx
			local yoffset = i*refdiry
			createObject(18450,x+5*refdirx+xoffset,y+5*refdiry+yoffset,z+20*height,Rotation2-i*Rotation2,0,rotz)
		end
		local reference5 = createObject(18450,x+6*refdirx,y+6*refdiry,z+20*height,0,0,rotz) -- reference object for the start of the twist
		return reference5
	end]]

	if (counter == 3) then -- straight section
		createMarker(x+refdirx,y+refdiry,z,"checkpoint",8)
		for i=2,5,1 do
		createObject(18450,x+i*refdirx,y+i*refdiry,z,0,0,rotz)
		end
		reference1 = createObject(18450,x+5*refdirx,y+5*refdiry,z,0,0,rotz)
		return reference1
	end
end


function chainelements(startelement)
	for i=1,20,1 do -- function that binds multiple sections together
		elementCounter = elementCounter + 1
		local randomcounter = math.random(1,3)
		if (turnCooldown > 0) then
			randomcounter = 3
			turnCooldown = turnCooldown - 1
		end
		if (consecutiveTurns >= maxTurns or edgeTurns >= maxTurns) then
			randomcounter = 3
			turnCooldown = maxTurns
			if (edgeTurns >= maxTurns) then
				edgeTurns = 0
			end
		end
		if (randomcounter == 1) then
			consecutiveTurns = consecutiveTurns + 1
		else
			consecutiveTurns = 0
		end
		--local randomcounter = 3
		startelement = createRandomElement(randomcounter,startelement)
		--outputChatBox(randomcounter)
	end
	
	-- create the ending
	local x,y,z = getElementPosition(startelement)
	local rotx,roty,rotz = getElementRotation(startelement)

	local refdirx = 80*math.cos(math.rad(rotz))
	local refdiry = 80*math.sin(math.rad(rotz))
	createObject(18450,x+refdirx,y+refdiry,z,0,0,rotz)
	createObject(18450,x+2*refdirx,y+2*refdiry,z,0,0,rotz)
	createObject(18450,x+3*refdirx,y+3*refdiry,z,0,0,rotz)
	
	teleportmarker = createMarker(x+3.5*refdirx,y+3.5*refdiry,z,"checkpoint",10,255,0,0) -- when reaching this marker the player is teleported to the actual end checkpoint
	
end
chainelements(getElementByID("start"))


function MarkerHit( hitElement, matchingDimension ) -- define MarkerHit function for the handler
    if (getElementType( hitElement ) == "vehicle") then
	setElementPosition(hitElement,-184.2,1886,115)
	end
end
addEventHandler( "onMarkerHit", teleportmarker, MarkerHit ) -- attach onMarkerHit event to MarkerHit function

function MarkerHit( hitElement, matchingDimension ) -- define MarkerHit function for the handler
    if (getElementType( hitElement ) == "vehicle") then
		if (math.random(0,1) < 0.25) then
			fixVehicle(hitElement)
		end
	
		if (getMarkerType(source) =="corona") then
			addVehicleUpgrade (hitElement,1008)
		end
	end
end
addEventHandler( "onMarkerHit", root, MarkerHit ) -- attach onMarkerHit event to MarkerHit function

setTimer(function()
	outputChatBox("This map follows a random path every time you play. If there's an impassable object, tough luck.",root,255,0,0, true)
	outputChatBox("Green markers will give you NITRO.",root,0,255,0, true)
end,2000,1)
