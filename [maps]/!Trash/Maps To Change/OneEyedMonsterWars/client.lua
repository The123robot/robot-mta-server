uniqueKey = tostring(getResourceName(getThisResource()))

txd = engineLoadTXD ( "sparrow.txd" )
dff = engineLoadDFF ( "sparrow.dff", 469 )
engineImportTXD ( txd, 469 )
engineReplaceModel ( dff, 469 )



function getPositionFromElementOffset(element,offX,offY,offZ)
	local m = getElementMatrix ( element )  -- Get the matrix
	local x = offX * m[1][1] + offY * m[2][1] + offZ * m[3][1] + m[4][1]  -- Apply transform
	local y = offX * m[1][2] + offY * m[2][2] + offZ * m[3][2] + m[4][2]
	local z = offX * m[1][3] + offY * m[2][3] + offZ * m[3][3] + m[4][3]
	return x, y, z                               -- Return the transformed point
end

function updateCamera ()
	local veh = getPedOccupiedVehicle(localPlayer)
	if 	veh then
		local x, y, z = getElementPosition (veh)
	end
	
	local a = getCameraTarget() --if the camera target is switched make set the camera matrix to follow that target instead
	if a then
		t=a
	end
	
	if isElement(t) then
		local x1,y1,z1 = getElementPosition(t)
		--local x2,y2,z2 = getPositionFromElementOffset(t,0,-10,0)
		if (z1 < 10000) then --make sure you cannot spectate players that are moved to 36000 when dead or spectating
			setCameraMatrix (x1,y1-20,z1+60,x1,y1,z1,0,180)
		end
	end
end
addEventHandler ( "onClientPreRender", getRootElement (), updateCamera )


local material =  dxCreateTexture("black.png")
local screenX,screenY = guiGetScreenSize()
local scale = screenX/4
function draw()
	for i,o in ipairs (getElementsByType("object")) do
		if getElementModel(o) == 3095 then
			local x,y,z = getElementPosition(o)
			dxDrawMaterialLine3D(x-2.7,y,z+0.6,x+2.7,y,z+0.6,material,6.5,tocolor(255,0,0,150),x,y,z-10)
		end
		
		if getElementModel(o) == 16442 then
			local x,y,z = getElementPosition(o)
			dxDrawMaterialLine3D(x-2.7,y,z-1.4,x+2.7,y,z-1.4,material,6.5,tocolor(0,255,0,255),x,y,z-10)
		end
		
		if getElementModel(o) == 16778 then
			local x,y,z = getElementPosition(o)
			dxDrawMaterialLine3D(x-2.7,y,z+3.6,x+2.7,y,z+3.6,material,6.5,tocolor(0,255,0,255),x,y,z-10)
		end
		
		if getElementModel(o) == 10281 then
			local x,y,z = getElementPosition(o)
			dxDrawMaterialLine3D(x-2.7,y,z+0.6-1.5,x+2.7,y,z+0.6-1.5,material,6.5,tocolor(0,255,0,255),x,y,z-10)
		end
		
		
	end
	
	if c_grid and p_grid and size then
		for a = 0,#c_grid do
			for b=0,#c_grid do
				local red,green,blue,alpha = 0,0,0,150
				if c_grid[a][b] then
					red = 255
					alpha = 255
				end
				if p_grid[a][b] then
					green = 255
					alpha = 255
				end
				
				dxDrawRectangle (20+a*size,screenY-40-size-b*size, size,size, tocolor (red,green,blue,alpha) )
			end
		end
	end
	
end
addEventHandler("onClientRender",root,draw)


function updateGrid(grid,powerUpPositions)
	c_grid = grid
	p_grid = powerUpPositions
	size = (screenX/5)/#c_grid
	
	setPlayerHudComponentVisible("radar",false)
end
addEvent("updateGrid",true)
addEventHandler("updateGrid",root,updateGrid)
