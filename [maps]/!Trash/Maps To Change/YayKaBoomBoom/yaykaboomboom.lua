function checkCheckpoint(checkpoint, time)
    local veh = getPedOccupiedVehicle(source)
    -- Set the ZR-350's license plate to a "random" one if the player's checkpoint count is less than 2 -- This doesn't work! Will need to do on resource start stuff, or try to set license plate back to player name after Tampa!
    if (checkpoint < 2) then
        onStart(veh)
    end
    -- Set the player's vehicle to Tampa when they reach checkpoint 2
    if (checkpoint == 2) then
        onTampa(veh)
    end
    -- Set the player's vehicle to Bike when they reach checkpoint 5 as a substitute for running on-foot
    if (checkpoint == 5) then
        onBike(veh)
    end
    -- Set the Blade's license plate to a "random" one if the player's checkpoint count is equal to or greater than 6
    if (checkpoint >= 6) then
        onEnd(veh)
    end
end

function onStart(veh)
    setVehiclePlateText(veh, "970IBJH")
end

function onEnd(veh)
    setVehiclePlateText(veh, "C1854JR")
end

function onTampa(veh)
    setElementModel(veh, 549) -- (vehicle to change, vehicle ID) -- Set the player's vehicle to Tampa
    setVehiclePlateText(veh, "TIMEBOMB") -- (vehicle to change, string) -- Set the Tampa's license plate to TIMEBOMB as seen in singleplayer
    setElementPosition(veh, -1699, 1036, 46) -- (vehicle to change, posX, posY, posZ)
    setElementRotation(veh, 0, 0, 180) -- (vehicle to change, rotX, rotY, rotZ)
    setElementVelocity(veh, 0, 0, 0) -- (vehicle to change, velX, velY, velZ)
end

function onBike(veh)
    setElementModel(veh, 509) -- (vehicle to change, vehicle ID) -- Set the player's vehicle to Bike
    setElementPosition(veh, -2180, -262, 37) -- (vehicle to change, posX, posY, posZ)
    setElementRotation(veh, 0, 0, 270) -- (vehicle to change, rotX, rotY, rotZ)
    setElementVelocity(veh, 0, 0, 0) -- (vehicle to change, velX, velY, velZ)
end

addEvent('onPlayerReachCheckpoint')
addEventHandler('onPlayerReachCheckpoint', getRootElement(), checkCheckpoint)
