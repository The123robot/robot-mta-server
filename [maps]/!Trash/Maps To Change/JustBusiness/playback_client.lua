GhostPlayback = {}
GhostPlayback.__index = GhostPlayback

addEvent( "onClientGhostDataReceive2", true )
-- addEvent( "clearMapGhost", true )

-- function GhostPlayback:create( recording, ped, vehicle )
	-- local result = {
		-- ped = ped,
		-- vehicle = vehicle,
		-- recording = recording,
		-- isPlaying = false,
		-- startTick = nil,
	-- }
	-- setElementCollisionsEnabled( result.ped, false )
	-- setElementCollisionsEnabled( result.vehicle, false )
	-- setVehicleFrozen( result.vehicle, true )
	-- setElementAlpha( result.ped, 0 )
	-- setElementModel( result.ped, 269)
	-- setElementAlpha( result.vehicle, 0 )
	
	-- --local x,y,z = getElementPosition(result.vehicle)
	-- --local playerVehicle = getPedOccupiedVehicle(localPlayer)
	-- --setElementPosition(playerVehicle,x+5,y,z)
	-- --attachElements(playerVehicle,result.vehicle)
	
	
	-- return setmetatable( result, self )
-- end

-- function checkVehicles(vehicle,seat,player)
	-- if (source==localPlayer) and not warp then
		-- if globalVeh then
			-- warp = true
			-- attachElements(vehicle,globalVeh)
		-- else
			-- setTimer(checkVehicles,200,1,vehicle,0,source)
		-- end
	-- end
-- end
-- addEventHandler("onClientPlayerVehicleEnter",getRootElement(),checkVehicles)

-- function GhostPlayback:destroy( finished )
	-- self:stopPlayback( finished )
	-- if self.checkForCountdownEnd_HANDLER then removeEventHandler( "onClientRender", root, self.checkForCountdownEnd_HANDLER ) self.checkForCountdownEnd_HANDLER = nil end
	-- if self.updateGhostState_HANDLER then removeEventHandler( "onClientRender", root, self.updateGhostState_HANDLER ) self.updateGhostState_HANDLER = nil end
	-- if isTimer( self.ghostFinishTimer ) then
		-- killTimer( self.ghostFinishTimer )
		-- self.ghostFinishTimer = nil
	-- end
	-- self = nil
-- end

function preparePlayback()
	addEventHandler( "onClientRender", root, checkForCountdownEnd )
end


function checkForCountdownEnd()
	for i,key in ipairs (disableKeys) do
		toggleControl(key,false)
	end
	
	for i,p in ipairs (getElementsByType("player")) do
		if p~=localPlayer then
			
			local car = getPedOccupiedVehicle(p)
			if car then
				setElementAlpha(car,0)
				setElementDimension(car,10)
				local occupants = getVehicleOccupants(car)
				if occupants[1] then
					setElementDimension(occupants[1],10)
				end
			end
			setElementAlpha(p,0)
			setElementDimension(p,10)
		end
	end
	
	for i,veh in ipairs (getElementsByType("vehicle")) do
		local oc = getVehicleOccupant(veh)
		if oc then
			if (getElementType(oc) ~= "player") then
				setElementAlpha(oc,0)
				setElementAlpha(veh,0)
			end
		end
	end
	
	
	local vehicle = getPedOccupiedVehicle( getLocalPlayer() )
	if vehicle then
		local frozen = isVehicleFrozen( vehicle )
		if not frozen then
			removeEventHandler( "onClientRender", root, checkForCountdownEnd)
			startPlayback()
		end
	end
end

function startPlayback()
	-- local x,y,z = getElementPosition(self.vehicle)
	-- local playerVehicle = getPedOccupiedVehicle(localPlayer)
	-- setElementPosition(playerVehicle,x,y,z)
	-- attachElements(playerVehicle,self.vehicle)
	
	local BigSmoke = createPed(269,1666,-1666.9,21.4,180)
	setPedControlState(BigSmoke,"forwards",true)
	playSFX("script", 168, 37, false)
	setTimer(playSFX,2400,1,"script", 168, 32, false)
	setTimer(playSFX,4000,1,"script", 168, 54, false)

	setTimer(function()
		destroyElement(BigSmoke)
		triggerServerEvent("makeVisible",localPlayer)
		startTick = getTickCount()
		isPlaying = true
		addEventHandler( "onClientRender", root, updateGhostState )
	end,5200,1)
end

function stopPlayback( finished )
	resetKeyStates()
	isPlaying = false
	removeEventHandler( "onClientRender", root,updateGhostState)
end

function updateGhostState()
	currentIndex = currentIndex or 1
	local ticks = getTickCount() - startTick
	setElementHealth( localPlayer, 100 ) -- we don't want the ped to die
	
	local vehicle = getPedOccupiedVehicle(localPlayer)
	if not vehicle then
		return
	end
	
	while (recording[currentIndex] and recording[currentIndex].t < ticks) do
		local theType = recording[currentIndex].ty
		if theType == "st" then
			-- Skip
		elseif theType == "po" then
			local x, y, z = recording[currentIndex].x,recording[currentIndex].y, recording[currentIndex].z
			local rX, rY, rZ = recording[currentIndex].rX, recording[currentIndex].rY, recording[currentIndex].rZ
			local vX, vY, vZ = recording[currentIndex].vX, recording[currentIndex].vY, recording[currentIndex].vZ
			local health = recording[currentIndex].h or 1000
			setElementPosition( vehicle, x, y, z )
			setElementRotation( vehicle, rX, rY, rZ )
			setElementVelocity( vehicle, vX, vY, vZ )
			setElementHealth( vehicle, health )
		elseif theType == "k" then
			local control = recording[currentIndex].k
			local state = recording[currentIndex].s
			setControlState(control, state )
		end
		currentIndex = currentIndex + 1
		
		if not recording[currentIndex] then
			stopPlayback( true )
		end
	end
end

function resetKeyStates()
	--outputChatBox("finshed, keystates reset")
	for i,key in ipairs (disableKeys) do
		toggleControl(key,true)
	end
end


disableKeys = {"vehicle_left","vehicle_right","steer_forward","steer_back","accelerate","brake_reverse","handbrake"}

function receiveRecording(s_recording)
	recording = s_recording			
	
	--outputChatBox("data received, disabling keys")
	for i,key in ipairs (disableKeys) do
		toggleControl(key,false)
	end
	
	
	preparePlayback()
end
addEventHandler( "onClientGhostDataReceive2", root,receiveRecording)

-- addEventHandler( "clearMapGhost", root,
	-- function()
		-- if playback then
			-- playback:destroy()
			-- globalInfo.bestTime = math.huge
			-- globalInfo.racer = ""
		-- end
	-- end
-- )