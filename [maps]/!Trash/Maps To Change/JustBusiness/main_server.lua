function start(newstate,oldstate) --function to update the race states
	if (newstate == "GridCountdown") then
		for index,theElement in ipairs (getElementsByType("object")) do
			if (getElementModel(theElement) == 3458) then
				setElementAlpha(theElement,0)
			end
		end
		
		for index,thePlayer in ipairs (getElementsByType("player")) do
			setPedDoingGangDriveby(thePlayer,true)
		end
		
	end
	
	if (newstate == "Running" and oldstate == "GridCountdown") then
		for index,thePlayer in ipairs (getElementsByType("player")) do
			giveWeapon(thePlayer,31,9999,true)
		end
	end
end
addEvent ( "onRaceStateChanging", true )
addEventHandler ( "onRaceStateChanging", getRootElement(), start )

firstTime = {}
function enterVehicle ( thePlayer, seat, jacked ) -- when a player enters a vehicle
	if not firstTime[thePlayer] and (getElementType(thePlayer)=="player") then
		firstTime[thePlayer] = true
		--warpPedIntoVehicle (thePlayer,source,1)
		local bigSmoke = createPed(269,0,0,0)
		setElementAlpha(bigSmoke,0)
		--warpPedIntoVehicle(thePlayer,source,1)
		warpPedIntoVehicle(bigSmoke,source,1)
		--setElementDimension(source,0)
		giveWeapon(thePlayer,31,0,true)
		setPedStat(thePlayer, 78, 1000)
		setWeaponProperty(31,"pro","damage",100)
		setWeaponProperty(31,"pro","maximum_clip_ammo",100)
		setWeaponProperty(31,"pro","weapon_range",500)

		-- local v = source
		-- setTimer(function()
		-- 	setElementDimension(v,0)
		-- 	setElementAlpha(v,0)
		-- end,1000,0)

	end
end
addEventHandler ( "onVehicleEnter", getRootElement(), enterVehicle ) 

--warpPedIntoVehicle (localPlayer,getPedOccupiedVehicle(localPlayer),1)

function visibleHandler()
	local o = getVehicleOccupants(getPedOccupiedVehicle(source))
	setElementAlpha(o[1],255)
end
addEvent("makeVisible",true)
addEventHandler("makeVisible",root,visibleHandler)

function checkpointcounter(checkpoint,time_)
	if checkpoint == 18 then
		setWeaponAmmo(source,31,0)
	end
	triggerClientEvent(source,"markertriggers",source,checkpoint) -- trigger marker triggers clientside
end
addEvent("onPlayerReachCheckpoint")
addEventHandler("onPlayerReachCheckpoint",root,checkpointcounter)


function loadNewVehicle(finalVehicle)
	local veh = getElementByID(finalVehicle)
	local x,y,z = getElementPosition(veh)
	local rotx,roty,rotz = getElementRotation(veh)
	local vehicleID = getElementModel(veh)
	
	local playerVeh = getPedOccupiedVehicle(source)
	local occupants = getVehicleOccupants(playerVeh)
	if occupants[1] then
		destroyElement(occupants[1])
	end
	
	setElementModel(playerVeh,vehicleID)
	setElementPosition(playerVeh,x,y,z)
	setElementRotation(playerVeh,rotx,roty,rotz)
	--warpPedIntoVehicle(source,playerVeh,0)
end
addEvent("onSmokeFinish",true)
addEventHandler("onSmokeFinish",root,loadNewVehicle)

-- prevent weaponfire from synching with other players
addEventHandler( "onWeaponFire", root,
    function ()
        cancelEvent()
    end
)

