function markertriggers(markercounter)
	
	-- voice triggers
	if markercounter == 1 then
		playSFX("script", 168, 62, false)
		setTimer(playSFX,3500,1,"script", 168, 64, false)
	end
	
	if markercounter == 2 then
		playSFX("script", 168, 67, false)
	end
	
	if markercounter == 4 then
		playSFX("script", 168, 63, false)
	end
	
	if markercounter == 5 then
		setTimer(playSFX,500, 1,"script", 168, 66, false)
		setTimer(playSFX,2000, 1,"script", 168, 26, false)
		setTimer(playSFX,5000, 1,"script", 168, 71, false)
		setTimer(playSFX,6000,1,"script", 168, 72, false)
	end
	
	if markercounter == 6 then
		setTimer(playSFX,3000,1,"script", 168, 73, false)
		setTimer(playSFX,4000,1,"script", 168, 74, false)
	end
	
	if markercounter == 7 then
		setTimer(playSFX,1000,1,"script", 168, 75, false)
		setTimer(playSFX,2000,1,"script", 168, 76, false)
		setTimer(playSFX,6000,1,"script", 168, 77, false)
		setTimer(playSFX,11000,1,"script", 168, 78, false)
	end
	
	
	
	if markercounter == 8 then
		markertriggers(9)
		
		playSFX("script", 168, 79, false)
		setTimer(playSFX,3500,1,"script", 168, 80, false)
		setTimer(playSFX,7500,1,"script", 168, 81, false)
		setTimer(playSFX,10000,1,"script", 168, 82, false)
	end
	
	if markercounter == 11 then
		playSFX("script", 168, 84, false)
		setTimer(playSFX,2000,1,"script", 168, 86, false)
	end
	
	if (markercounter == 12) then
		playSFX("script", 168, 89, false)
		setTimer(playSFX,2500,1,"script", 168, 90, false)
		setTimer(playSFX,4500,1,"script", 168, 91, false)
		setTimer(playSFX,6000,1,"script", 168, 92, false)
		setTimer(playSFX,7000,1,"script", 168, 93, false)
	end
	
	if (markercounter == 13) then
		playSFX("script", 168, 94, false)
		setTimer(playSFX,4000,1,"script", 168, 95, false)
		setTimer(playSFX,6000,1,"script", 168, 96, false)
		setTimer(playSFX,9000,1,"script", 168, 105, false)
		setTimer(playSFX,10500,1,"script", 168, 106, false)
	end
	
	if (markercounter == 14) then
		playSFX("script", 168, 20, false)
		setTimer(playSFX,1400,1,"script", 168, 22, false)
	end
	
	if markercounter == 15 then
		playSFX("script", 168, 110, false)
		setTimer(playSFX,1500,1,"script", 168, 111, false)
		setTimer(playSFX,4000,1,"script", 168, 113, false)
		
		setTimer(function()
			local veh = getElementByID("explosion1")
			local x,y,z = getElementPosition(veh)
			createExplosion(x,y,z,10)
		end,4500,1)
	end
	
	if markercounter == 16 then
		playSFX("script", 168, 114, false)
		setTimer(playSFX,2000,1,"script", 168, 115, false)
		setTimer(playSFX,3200,1,"script", 168, 117, false)
	end
	
	if markercounter == 17 then
		playSFX("script", 168, 126, false)
	end
	
	if markercounter == 18 then
		playSFX("script", 168, 127, false)
		setTimer(playSFX,1300,1,"script", 168, 128, false)
		setTimer(playSFX,2300,1,"script", 168, 129, false)
		setTimer(playSFX,4300,1,"script", 168, 130, false)
		setTimer(playSFX,6500,1,"script", 168, 131, false)
		setTimer(playSFX,7500,1,"script", 168, 132, false)
		setTimer(playSFX,10500,1,"script", 168, 133, false)
		setTimer(changeVehicle,10500,1)
	end
	
	local spawns = {}
	local i = 1
	repeat
		local spawn = getElementByID("spawn"..tostring(markercounter).."-"..tostring(i))
		if not spawn then 
			break 
		end
		setElementAlpha(spawn,0)
		table.insert(spawns,spawn)
		i = i+1
	until not spawn
	
	for index,sp in ipairs (spawns) do 
		local x,y,z = getElementPosition(sp)
		local rotx,roty,rotz = getElementRotation(sp)
		local vehicleID = getElementModel(sp)
		local ped = createPed (math.random(111,113),x,y,z)
		local ped2 = createPed (math.random(111,113),x,y,z)
		local veh = createVehicle(vehicleID,x,y,z,rotx,roty,rotz)
		setElementData (veh, 'race.collideothers', 1 )		
		warpPedIntoVehicle (ped, veh)
		warpPedIntoVehicle (ped2,veh,1)
		setPedControlState(ped,"accelerate",true)
	end
end
addEvent("markertriggers",true)
addEventHandler ( "markertriggers", getRootElement(), markertriggers )


vehicles = {}
vehicles[17] = "buffalo"
vehicles[16] = "superGT"
vehicles[15] = "bloodring"
vehicles[14] = "landstalker"
vehicles[13] = "firetruck"
vehicles[12] = "esperanto"
vehicles[11] = "yosemite"
vehicles[10] = "flatbed"
vehicles[9]  = "camper"
vehicles[8]  = "vortex"
vehicles[7]  = "tractor"
vehicles[6]  = "mower"

function changeVehicle()
	local playerVeh = getPedOccupiedVehicle(localPlayer)
	local finalVehicle
	if (score <= 12000) then
		finalVehicle = "mower"
	elseif (score >= 36000) then
		finalVehicle = "infernus"
	else
		finalVehicle = vehicles[math.floor(score/2000)]
	end
	
	local x,y,z = getElementPosition(playerVeh)
	local rx,ry,rz = getElementRotation(playerVeh)
	local model = getElementModel(playerVeh)
	local newveh = createVehicle(model,x,y,z,rx,ry,rz)
	local ped = createPed(269,x,y,z)
	warpPedIntoVehicle(ped,newveh)
	
	for i,veh in ipairs (getElementsByType("vehicle")) do
		setElementDimension(veh,0)
		local occupant = getVehicleOccupant(veh)
		if occupant then
			setElementDimension(occupant,0)
		end
	end
	
	triggerServerEvent("onSmokeFinish",localPlayer,finalVehicle)
end


------------
-- Score
------------

score = 0
setElementData(localPlayer,"score",0)
function noDeagle(_,_,_,_,_,_,hitElement)
	if (source == localPlayer) and isElement(hitElement) then
		local points = 0
		if getElementType(hitElement) == "vehicle" then
			if (getElementModel(hitElement)== 443) then
				points = 10 --packer
			end
			if (getElementModel(hitElement)== 426) or (getElementModel(hitElement)== 405) or (getElementModel(hitElement)== 490) then
				points = 50 --premier,sentinel,rancher
			end
			if getElementModel(hitElement)== 463 then
				points = 100 --bikes
			end
		end
		if getElementType(hitElement) == "ped" then
			if not isPedDead(hitElement) then
				points = 100
			end
			if getElementModel(hitElement)==269 then
				points = -1000
			end
		end
		if getElementType(hitElement) == "object" then
			if getElementModel(hitElement)== 1225 then
				points = 20 --barrel
			end
			if getElementModel(hitElement)== 1483 then
				points = 2000
			end
		end
		score = score+points
		setElementData(localPlayer,"score",score)
	end
end
addEventHandler("onClientPlayerWeaponFire", getRootElement(), noDeagle)

function vehicleExplode()
	local model = getElementModel(source)
	if (model == 443) or (model == 426) or (model == 405) or (model == 490) or (model == 463) then
		score = score+500
		setElementData(localPlayer,"score",score)
	end
end
addEventHandler("onClientVehicleExplode", getRootElement(),vehicleExplode)

function onWasted(killer, weapon, bodypart)
	if (getElementType(source) == "ped") then
		if getPedOccupiedVehicle(source) then
			local x,y,z = getElementPosition(source)
			local ID = getElementModel(source)
			destroyElement(source)
			local ped = createPed(ID,x,y,z)
			setElementHealth(ped,0)
		end
		score = score+500
		setElementData(localPlayer,"score",score)
	end
end
addEventHandler("onClientPedWasted", getRootElement(), onWasted)


function comp(a,b)
	if not getElementData(a,"score") then return false end
	if not getElementData(b,"score") then return true end
	
	if (getElementData(a,"score")) > (getElementData(b,"score")) then
		return true
	else
		return false
	end
end

local screenx,screeny = guiGetScreenSize()
function displayScore()
	 --dxDrawText (score, 50,screeny/2, screenx, screeny, tocolor ( 255, 0, 0, 255 ), 2, "pricedown" )
	 dxDrawText(score,screenx/2,screeny*4/5,screenx/2,screeny*4/5,tocolor(255,0,0,255),2,"pricedown","left","center",false,false,false,true)
	 
	local players = getElementsByType("player")
	table.sort(players,comp)
	
	for i,player in ipairs(players) do
		if (i<11) then
			local score = getElementData(player,"score")
			if score then
				dxDrawText(i .. ". " .. getPlayerName(player) .. "#FFFFFF     ",screenx*2/3,screeny/4+i*30,screenx*2/3,screeny/4+i*30,tocolor(255,255,255,255),1,"pricedown","left","center",false,false,false,true)
				dxDrawText(score,screenx*2/3+350,screeny/4+i*30,screenx*2/3+350,screeny/4+i*30,tocolor(255,0,0,255),1,"pricedown","center","center",false,false,false,true)
			end
		end
	end
end
addEventHandler("onClientRender",root,displayScore)








-- function shootingPed(ped,veh)
	-- outputChatBox("received")
	-- warpPedIntoVehicle (ped,veh,1)
	-- setPedDoingGangDriveby(ped,true)
-- end
-- addEvent("loadShootingPed",true)
-- addEventHandler("loadShootingPed",getRootElement(),shootingPed)
