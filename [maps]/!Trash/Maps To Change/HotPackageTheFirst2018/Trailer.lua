local trailer
local vehicle
local rock
local drawText = false
local screenWidth, screenHeight = guiGetScreenSize ( )

function getPositionFromElementOffset(element,offX,offY,offZ)
	local m = getElementMatrix ( element )  -- Get the matrix
	local x = offX * m[1][1] + offY * m[2][1] + offZ * m[3][1] + m[4][1]  -- Apply transform
	local y = offX * m[1][2] + offY * m[2][2] + offZ * m[3][2] + m[4][2]
	local z = offX * m[1][3] + offY * m[2][3] + offZ * m[3][3] + m[4][3]
	return x, y, z                               -- Return the transformed point
end

addEventHandler("onClientVehicleEnter",  getRootElement(), function(thePlayer) 
	if thePlayer ==  getLocalPlayer() then
		killTimer(attachTrailer)
		killTimer(checkAttached)
		setTimer(attachTrailer, 1000, 1)
	else
		local otherVehicle = getPedOccupiedVehicle(thePlayer)
	
		local rotX, rotY, rotZ = getElementRotation(otherVehicle)
	
		local x,y,z = getPositionFromElementOffset(otherVehicle,0,-10,0)
		
		local otherTrailer = createVehicle ( 435, x, y, z, rotX, rotY, rotZ )
		attachTrailerToVehicle ( otherVehicle, otherTrailer )   -- attach trailer to truck
		setElementAlpha(otherTrailer, 155)	-- make trailer semi-transparent
	end
end 
)

addEventHandler ( "onClientPlayerWasted", getRootElement(), function()
	if source == getLocalPlayer() then
		destroyElement(trailer)
		destroyElement(rock)
	else
		destroyElement(getVehicleTowedByVehicle(getPedOccupiedVehicle(source)))
	end
end
)

function attachTrailer()
	vehicle = getPedOccupiedVehicle(localPlayer)
	
	local rotX, rotY, rotZ = getElementRotation(vehicle)
	
	local x,y,z = getPositionFromElementOffset(vehicle,0,-10,0)
	
	--triggerServerEvent( "createTrailer", resourceRoot, x, y, z, rotX, roty, royZ)
	trailer = createVehicle ( 435, x, y, z, rotX, rotY, rotZ )    -- create a trailer
	rock = createObject (1305, x, y, z)		-- create a rock
	setElementCollisionsEnabled(rock, false)	-- make rock uncolideable
	setElementAlpha(rock, 0)		-- make rock invisible
	attachElements(rock,trailer,0,0,0)	-- attach rock to trailer
	attachTrailerToVehicle ( vehicle, trailer )   -- attach trailer to truck
	setElementAlpha(trailer, 155)	-- make trailer semi-transparent
	
	setTimer(checkAttached, 1000, 1)
end

function checkAttached()
	if getElementData(localPlayer, "race.spectating") == false then
		vehX, vehY, vehZ = getElementPosition(vehicle)
		traX, traY, traZ = getElementPosition(rock)
		
		distance = getDistanceBetweenPoints3D(vehX, vehY, vehZ, traX, traY, traZ)

		if (distance > 15) then
			setElementHealth(localPlayer, 0)
			drawText = true
			setTimer(stopDrawing, 5000, 1)
		else 
			setTimer(checkAttached, 1000, 1)
		end
	end
end

function stopDrawing()
	drawText = false
end

function drawing()
	local screenWidth,screenHeight = guiGetScreenSize() 

	local textSize = (screenHeight / 1080) * 1.5

	posX = screenWidth / 2
	posY = screenHeight / 2

	local text = ""

	if drawText then
		text = "Keep your trailer attached!"
	end

	width = dxGetTextWidth(text, textSize, "pricedown")
	dxDrawBorderedText (1, text, posX - (width / 2), posY, 1000, 1000, tocolor ( 255, 255, 255, 255 ), textSize, "pricedown")
end
addEventHandler ( "onClientRender", root, drawing ) -- keep the text visible with onClientRender.

function dxDrawBorderedText (outline, text, left, top, right, bottom, color, scale, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    local outline = (scale or 1) * (1.333333333333334 * (outline or 1))
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outline, top - outline, right - outline, bottom - outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outline, top - outline, right + outline, bottom - outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outline, top + outline, right - outline, bottom + outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outline, top + outline, right + outline, bottom + outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left - outline, top, right - outline, bottom, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left + outline, top, right + outline, bottom, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left, top - outline, right, bottom - outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text:gsub("#%x%x%x%x%x%x", ""), left, top + outline, right, bottom + outline, tocolor (0, 0, 0, 225), scale, font, alignX, alignY, clip, wordBreak, postGUI, false, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
    dxDrawText (text, left, top, right, bottom, color, scale, font, alignX, alignY, clip, wordBreak, postGUI, colorCoded, subPixelPositioning, fRotation, fRotationCenterX, fRotationCenterY)
end