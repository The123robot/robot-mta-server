local target
localPlayer = getLocalPlayer()

function determineTarget()
	local aimingAt = getPedTarget(localPlayer)
	if aimingAt then
		if getElementType(aimingAt) == "ped" then
			return aimingAt
		end
		if getElementType(aimingAt) == "vehicle" then
			if not getVehicleOccupant(aimingAt) then
				return aimingAt
			end
		end
	end
	return false
end


function find()
	if not target then
		target = determineTarget()
	else
		target = false
	end
end


function push()
	if not isElement(target) then target = determineTarget(localPlayer) end
	if not target then return end
	local x1,y1,z1 = getElementPosition(localPlayer)
	local x2,y2,z2 = getElementPosition(target)
	x2,y2,z2 = x2-x1,y2-y1,z2-z1
	local spd = 1/math.sqrt(x2*x2+y2*y2+z2*z2)
	x2,y2,z2 = x2*spd,y2*spd,z2*spd
	setElementVelocity(target,x2,y2,z2)
	target = false
end

addEventHandler("onClientPreRender",root,function()
	if target then
		local x1,y1,z1 = getPedTargetStart(localPlayer)
		local x2,y2,z2 = getPedTargetEnd(localPlayer)
		x2,y2,z2 = x2-x1,y2-y1,z2-z1
		local dist = 5/math.sqrt(x2*x2+y2*y2+z2*z2)
		x2,y2,z2 = x2*dist,y2*dist,z2*dist
		local fx,fy,fz = x1+x2,y1+y2,z1+z2
		local cx,cy,cz = getElementPosition(target)
		x2,y2,z2 = fx-cx,fy-cy,fz-cz
		local spd = math.min(0.05*math.sqrt(x2*x2+y2*y2+z2*z2),0.1)
		x2,y2,z2 = x2*spd,y2*spd,z2*spd
		setElementVelocity(target,x2,y2,z2)
	end
end)

local enabled = false
function enableGun()
	if not enabled then
		bindKey("mouse2","down",find)
		bindKey("mouse1","down",push)
		outputChatBox("You found the GRAVITY GUN! Use RMB to drag and drop vehicles, LMB to push",255,0,0)
	end
	enabled = true
end
addEvent("hl.gravityGun",true)
addEventHandler("hl.gravityGun", getRootElement(), enableGun )


function disableGun()
	unbindKey("mouse2","down",find)
	unbindKey("mouse1","down",push)
	enabled = false
end
addEvent("hl.gravityGunOff",true)
addEventHandler("hl.gravityGunOff", getRootElement(), disableGun )