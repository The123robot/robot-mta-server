function start(newstate,oldstate) --function to update the race states
	setElementData(root,"hl.racestate",newstate,true)
	if (newstate == "LoadingMap") then 
		
		-- spawning trains off the tracks
		local trains = {590,569,570,537}
		for index,theVehicle in ipairs (getElementsByType("vehicle")) do
			if (getElementModel(theVehicle) == 583) then
				--setElementFrozen(theVehicle,true)
				setTrainDerailed(theVehicle,true)
				setElementModel(theVehicle,trains[math.random(1,#trains)])
			end
		end
		---
		
		-- creating water at the start
		local x1,y1,z1 = getElementPosition(getElementByID("water1-4"))
		local x2,y2,z2 = getElementPosition(getElementByID("water1-3"))
		local x3,y3,z3 = getElementPosition(getElementByID("water1-2"))
		local x4,y4,z4 = getElementPosition(getElementByID("water1-1"))
		local height = 7.9
		createWater(x1,y1,height,x2,y2,height,x3,y3,height,x4,y4,height)
		createWater(x1,y1-200,height,x2-50,y2-200,height,x1,y1,height,x2-50,y2,height)
		
		waterHeights = {0,3.5,0.3,1.3}
		for i=2,4 do
			local x1,y1,z1 = getElementPosition(getElementByID("water"..tostring(i).."-1"))
			local x2,y2,z2 = getElementPosition(getElementByID("water"..tostring(i).."-2"))
			local x3,y3,z3 = getElementPosition(getElementByID("water"..tostring(i).."-3"))
			local x4,y4,z4 = getElementPosition(getElementByID("water"..tostring(i).."-4"))
			local height = waterHeights[i]
			createWater(x1,y1,height,x2,y2,height,x3,y3,height,x4,y4,height)
		end
		setWaterColor(107,71,37)
		---
		
		-- creating combine cops
		for index,thePed in ipairs (getElementsByType("ped")) do
			if (getElementModel(thePed) == 121) then
				setElementModel(thePed,math.random(22,25))
				giveWeapon(thePed,31,9999,true)
			end
		end
		
		for index,v in ipairs (getElementsByType("vehicle")) do
			if (getElementModel(v) == 584) then
				setElementFrozen(v,true)
			end
		end
		
	end
	
	-- adding handler for teleporting in the tunnel
	if (newstate == "GridCountdown") then 
		teleport1 = createColCircle(2581.7,-2125,5)
		addEventHandler("onColShapeHit",teleport1,teleport)
		
		colshape1 = createColTube(-705,1978.9,0,10,200)
		addEventHandler("onColShapeHit",colshape1,setGravity)
		addEventHandler("onColShapeLeave",colshape1,returnGravity)
		
		-- set all vehicles that are placed on the map in the editor collisions enabled
		for index,theVehicle in ipairs (getElementsByType("vehicle")) do
			setVehicleSirensOn(theVehicle, true)
			if not (getVehicleOccupant(theVehicle)) then
				setElementData ( theVehicle, 'race.collideothers', 1 ) -- if the vehicle is empty, make it collide with other vehicles
            end
		end
		
	end

	if (newstate == "Running" and oldstate == "GridCountdown") then
	end
end
addEvent ( "onRaceStateChanging", true )
addEventHandler ( "onRaceStateChanging", getRootElement(), start )

function enterVehicle ( thePlayer, seat, jacked ) -- when a player enters a vehicle
    setElementModel(thePlayer,20)
end
addEventHandler ( "onVehicleEnter", getRootElement(), enterVehicle )

-- function to teleport a player through the tunnel
function teleport(hitElement)
	if (getElementType(hitElement) == "vehicle") then
		if (getElementType(getVehicleOccupant(hitElement)) == "player") then
			--local rotx,roty,rotz = getElementRotation(hitElement)
			local velx,vely,velz = getElementVelocity(hitElement)
			fadeCamera(getVehicleOccupant(hitElement),false,3)
			setTimer(function()
				setElementPosition(hitElement,355.7,2643.6,18)
				setElementModel(hitElement,568)
			end,3000,1)
			
			setTimer(fadeCamera,3500,1,getVehicleOccupant(hitElement),true,5)
			--setElementRotation(hitElement,rotx,roty,rotz-90)
			setElementVelocity(hitElement,velx,vely,velz)
		end
	end
end

function setGravity(hitElement)
	if (getElementType(hitElement) == "vehicle") then
		local occupant = getVehicleOccupant(hitElement)
		if occupant then
			if (getElementType(occupant) == "player") then
				setPedGravity(getVehicleOccupant(hitElement),-0.005)
			end
		end
	end
end

function returnGravity(hitElement)
	if (getElementType(hitElement) == "vehicle") then
		local occupant = getVehicleOccupant(hitElement)
		if occupant then
			if (getElementType(occupant) == "player") then
				setPedGravity(getVehicleOccupant(hitElement),0.008)
			end
		end
	end
end

-- give all peds an SMG on race start
for index,thePed in ipairs (getElementsByType("ped")) do
	local model = getElementModel(thePed)
	if (model == 22) or (model == 23) or (model == 24) or (model == 25)then -- give the peds that have these models (sweet, bs and ryder) an ak, give the rest an smg
		giveWeapon(thePed,30,5000,true) 
	end
end

function regiveWeaponFunc (officer, weapon) -- regive weapon event, needed due to one bullet glitch
	if (weapon == nil) then				   -- triggered from clientside
		weapon = 30
	end
	giveWeapon(officer,weapon,1000,true)
end
addEvent("regiveweapon", true )
addEventHandler("regiveweapon", root, regiveWeaponFunc )

function ok(player)
	setPedDoingGangDriveby(player,true)
end
addCommandHandler("ok",ok)

function checkpointcounter(checkpoint,time_)
	if checkpoint > 10 and checkpoint <18 then
		giveWeapon(source,27,0,true)
		setPedDoingGangDriveby(source,true)
	else
		setPedDoingGangDriveby(source,false)
	end
	
	if checkpoint == 11 then
		triggerClientEvent(source,"hl.gravityGun",source)
	end
	
	if checkpoint == 18 then
		triggerClientEvent(source,"hl.gravityGunOff",source)
	end
	
	
	if checkpoint == 19 then
		local veh = getPedOccupiedVehicle(source)
		--setElementVelocity(veh,-0.12,0,1.1)
	end
	
	if checkpoint == 21 then
		local veh = getPedOccupiedVehicle(source)
		setElementVelocity(veh,-0.05,0.2,0.6)
	end
end
addEvent("onPlayerReachCheckpoint")
addEventHandler("onPlayerReachCheckpoint",root,checkpointcounter)



