-- this script draws the background images of the striders.

function load()
	strider1 = dxCreateTexture("images/strider1.png")
	gunship1= dxCreateTexture("images/gunship2.png")
	--gunship2= dxCreateTexture("images/gunship2.png")
end
addEventHandler("onClientResourceStart",getResourceRootElement(),load)

function drawImages()
	for index,m in ipairs (getElementsByType("object")) do
		if getElementModel(m)==1371 then
			local x,y,z = getElementPosition(m)
			dxDrawMaterialLine3D (x,y,z+18,x,y,z-2,strider1, 10, tocolor(255,255,255,255))
		end
		
		if getElementModel(m)==1370 then
			local x,y,z = getElementPosition(m)
			dxDrawMaterialLine3D (x,y,z+20,x,y,z,gunship1, 20, tocolor(255,255,255,255))
		end
	end
end
addEventHandler("onClientRender",root,drawImages)