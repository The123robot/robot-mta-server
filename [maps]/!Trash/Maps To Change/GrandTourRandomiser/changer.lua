local carids = {429, 541, 415, 480, 562, 494, 502, 503, 411, 559, 560, 506, 451, 558, 477 ,603, 402, 495}
local turboCount = 3

addEventHandler("onPlayerJoin",root,
function ()
	bindKey(source,"2","down",turbo)
	bindKey(source,"lctrl","down",turbo)
	bindKey(source,"rctrl","down",turbo)
	bindKey(source,"mouse1","down",turbo)
end)

addEventHandler("onResourceStart",resourceRoot,
function ()
	for index, player in ipairs(getElementsByType("player")) do	
	bindKey(player,"2","down",turbo)
	bindKey(player,"lctrl","down",turbo)
	bindKey(player,"rctrl","down",turbo)
	bindKey(player,"mouse1","down",turbo)
	end
	outputChatBox("#FF0000You have 3 turbos, use them wisely!" , root, 255, 0, 0, true ) 
end)

function randomCar(checkpoint, time)
	local veh = getPedOccupiedVehicle(source)
	setElementModel (veh, carids[math.random(#carids)])
end
addEvent('onPlayerReachCheckpoint')
addEventHandler('onPlayerReachCheckpoint', getRootElement(), randomCar)

function turbo(player)
	local veh = getPedOccupiedVehicle(player)
	if (veh) then
		if turboCount ~= 0 then
			addVehicleUpgrade (veh, 1010)
			turboCount = turboCount-1
			triggerClientEvent ( player, "hitmarker", player, turboCount)			
		end
		outputChatBox("#00FF00You have used " .. 3-turboCount .. " of 3 turbos" , root, 255, 0, 0, true ) 
	end
end

function makeMarkers()
	turboTopup1 = createMarker (-1609.4, -2718.4, 47.5, "cylinder", 3, 255, 255, 0, 50)
	turboTopup2 = createMarker (-1606.3, -2714, 47.5, "cylinder", 3, 255, 255, 0, 50)
	turboTopup3 = createMarker (-1603.5, -2709.4, 47.5, "cylinder", 3, 255, 255, 0, 50)
	turboTopupA = createMarker (-1606.3, -2714, 47, "cylinder", 15, 255, 255, 0, 20)
	turboTopup4 = createMarker (2837.5552, -1117.7128, 24, "cylinder", 3, 255, 255, 0, 50)
	turboTopup5 = createMarker (2848.5986, -1117.5001, 24, "cylinder", 3, 255, 255, 0, 50)
	turboTopup6 = createMarker (2843.0515, -1117.6271, 24, "cylinder", 3, 255, 255, 0, 50)
	turboTopupB = createMarker (2843.0515, -1117.6271, 24, "cylinder", 15, 255, 255, 0, 20)
	
end
addEventHandler("onResourceStart", getResourceRootElement(getThisResource()), makeMarkers)

function turboTopup(player, dimension)
	if source == turboTopup1 then
		if getElementType(player) == "player" then
			local vehh = getPedOccupiedVehicle(player)
			if vehh then
				addVehicleUpgrade (veh, 1010)
				turboCount = 3
				outputChatBox("#FF0000You have refueled your turbos!" , root, 255, 0, 0, true ) 
				triggerClientEvent ( player, "hitmarker", player, turboCount)	
			end
		end
	end
	if source == turboTopup2 then
		if getElementType(player) == "player" then
			local vehh = getPedOccupiedVehicle(player)
			if vehh then
				addVehicleUpgrade (veh, 1010)
				turboCount = 3
				outputChatBox("#FF0000You have refueled your turbos!" , root, 255, 0, 0, true ) 
				triggerClientEvent ( player, "hitmarker", player, turboCount)	
			end
		end
	end
	if source == turboTopup3 then
		if getElementType(player) == "player" then
			local vehh = getPedOccupiedVehicle(player)
			if vehh then
				addVehicleUpgrade (veh, 1010)
				turboCount = 3
				outputChatBox("#FF0000You have refueled your turbos!" , root, 255, 0, 0, true ) 
				triggerClientEvent ( player, "hitmarker", player, turboCount)	
			end
		end
	end
	if source == turboTopup4 then
		if getElementType(player) == "player" then
			local vehh = getPedOccupiedVehicle(player)
			if vehh then
				addVehicleUpgrade (veh, 1010)
				turboCount = 3
				outputChatBox("#FF0000You have refueled your turbos!" , root, 255, 0, 0, true ) 
				triggerClientEvent ( player, "hitmarker", player, turboCount)	
			end
		end
	end
	if source == turboTopup5 then
		if getElementType(player) == "player" then
			local vehh = getPedOccupiedVehicle(player)
			if vehh then
				addVehicleUpgrade (veh, 1010)
				turboCount = 3
				outputChatBox("#FF0000You have refueled your turbos!" , root, 255, 0, 0, true ) 
				triggerClientEvent ( player, "hitmarker", player, turboCount)	
			end
		end
	end
	if source == turboTopup6 then
		if getElementType(player) == "player" then
			local vehh = getPedOccupiedVehicle(player)
			if vehh then
				addVehicleUpgrade (veh, 1010)
				turboCount = 3
				outputChatBox("#FF0000You have refueled your turbos!" , root, 255, 0, 0, true ) 
				triggerClientEvent ( player, "hitmarker", player, turboCount)	
			end
		end
	end
end
addEventHandler('onMarkerHit', getRootElement(), turboTopup)