VEHICLE_ID = 602
TXD_FILE = "stinger.txd"
DFF_FILE = "stinger.dff"

addEventHandler('onClientResourceStart', resourceRoot, 
	function() 
		txd = engineLoadTXD ( TXD_FILE )
		engineImportTXD ( txd, VEHICLE_ID ) 
			
		dff = engineLoadDFF ( DFF_FILE, VEHICLE_ID ) 
		engineReplaceModel ( dff, VEHICLE_ID ) 
	end 
)

VEHICLE_ID_2 = 560
TXD_FILE_2 = "yakuza.txd"
DFF_FILE_2 = "yakuza.dff"

addEventHandler('onClientResourceStart', resourceRoot, 
	function() 
		txd = engineLoadTXD ( TXD_FILE_2 )
		engineImportTXD ( txd, VEHICLE_ID_2 ) 
			
		dff = engineLoadDFF ( DFF_FILE_2, VEHICLE_ID_2 ) 
		engineReplaceModel ( dff, VEHICLE_ID_2 ) 
	end 
)